/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.description;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.profession.ProfessionManager;
import com.crimsonrpg.api.profession.description.Description;
import com.crimsonrpg.api.profession.description.DescriptionLoader;
import com.crimsonrpg.api.profession.description.VariableGraph;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.profession.SimpleProfessionManager;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

/**
 *
 * @author simplyianm
 */
public class SkillDescriptionLoaderTest extends TestCase {
	private DescriptionLoader<Skill> instance;

	public SkillDescriptionLoaderTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite(SkillDescriptionLoaderTest.class);
		return suite;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		ProfessionManager manager = Mockito.mock(SimpleProfessionManager.class);
		CrimsonAPI.getInstance().setProfessionManager(manager);
		Mockito.when(manager.getGraph(Mockito.anyString())).thenReturn(new VariableGraph() {
			@Override
			public double getValue(double percent, double low, double high) {
				return low + ((high - low) * percent);
			}

		});
		instance = new SkillDescriptionLoader();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		instance = null;
	}

	/**
	 * Testing multiple variables.
	 */
	public void testMultipleVars() {
		System.out.println("loadDescription");

		int maxLevel = 5;
		String raw = "Deals 3...5 damage (linear) if you don't eat your face for 1000...9001 damage (linear)... We also have lots of other 7...2 damage (linear).";
		Description result = instance.loadDescription(maxLevel, raw);

		assertEquals(3.0, result.getVar("damage", 1));
		assertEquals(5, result.getVarInt("damage", 5));

		assertEquals(1000, result.getVarInt("damage1", 1));
		assertEquals(9001.0, result.getVar("damage1", 5));

		assertEquals(7.0, result.getVar("damage2", 1));
		assertEquals(2, result.getVarInt("damage2", 5));
		assertTrue(result.getVar("damage2", 3) < result.getVar("damage2", 1));
	}

	/**
	 * Testing spaces.
	 */
	public void testSpaces() {
		int maxLevel = 21;
		String raw = "Deals 4...44 ice cubes (linear) to the nearest vendor of doom.";
		Description result = instance.loadDescription(maxLevel, raw);

		assertEquals(4.0, result.getVar("ice cubes", 1));
		assertEquals(44, result.getVarInt("ice cubes", 21));
	}

	/**
	 * Testing style two:
	 * var #...# (graph)
	 */
	public void testStyleTwo() {
		int maxLevel = 5;
		String raw = "Summons a level 1...8 (linear) Horror.";
		Description result = instance.loadDescription(maxLevel, raw);

		assertEquals(1, result.getVarInt("level", 1));
		assertEquals(8, result.getVarInt("level", 5));
		assertEquals(4.5, result.getVar("level", 3));
	}

	/**
	 * Testing a hybrid combo of the two.
	 */
	public void testHybrid() {
		int maxLevel = 5;
		String raw = "Summons a level 1...8 (linear) Horror that can last for a maximum of 20...45 seconds (linear). Requires a captured soul.";
		Description result = instance.loadDescription(maxLevel, raw);

		assertEquals(1, result.getVarInt("level", 1));
		assertEquals(8, result.getVarInt("level", 5));
		assertEquals(4.5, result.getVar("level", 3));

		assertEquals(20, result.getVarInt("seconds", 1));
		assertEquals(45, result.getVarInt("seconds", 5));
	}

	/**
	 * Testing a hybrid combo of the two, with overlap.
	 */
	public void testHybridOverlap() {
		int maxLevel = 5;
		String raw = "Summons a level 1...8 (linear) Horror that can last for a maximum of 20...45 level (linear). Requires a captured soul.";
		Description result = instance.loadDescription(maxLevel, raw);

		assertEquals(1, result.getVarInt("level", 1));
		assertEquals(8, result.getVarInt("level", 5));
		assertEquals(4.5, result.getVar("level", 3));

		assertEquals(20, result.getVarInt("level1", 1));
		assertEquals(45, result.getVarInt("level1", 5));
	}

}
