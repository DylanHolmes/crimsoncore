/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.items;

import java.util.HashMap;
import java.util.Map;

import org.getspout.spoutapi.material.CustomItem;

import com.crimsonrpg.api.item.CrimsonItemManager;

/**
 *
 * @author simplyianm
 */
public class SimpleCrimsonItemManager implements CrimsonItemManager {
    private Map<String, CustomItem> customItems = new HashMap<String, CustomItem>();
    
    public void addItem(String name, CustomItem item) {
        if (customItems.containsKey(name)) {
            CrimsonItems.LOGGER.warning("[CrimsonItems] Duplicate item registration!");
        }
        customItems.put(name, item);
    }
    
    public CustomItem getItem(String name) {
        return customItems.get(name);
    }
}
