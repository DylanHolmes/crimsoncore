/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.items;

import java.util.HashMap;
import java.util.Map;

import org.getspout.spoutapi.material.CustomBlock;

import com.crimsonrpg.api.item.CrimsonBlockManager;

/**
 *
 * @author simplyianm
 */
public class SimpleCrimsonBlockManager implements CrimsonBlockManager {

    private Map<String, CustomBlock> customBlocks = new HashMap<String, CustomBlock>();

    public void addBlock(String name, CustomBlock Block) {
        if (customBlocks.containsKey(name)) {
            CrimsonItems.LOGGER.warning("[CrimsonBlocks] Duplicate Block registration!");
        }
        customBlocks.put(name, Block);
    }

    public CustomBlock getBlock(String name) {
        return customBlocks.get(name);
    }

}
