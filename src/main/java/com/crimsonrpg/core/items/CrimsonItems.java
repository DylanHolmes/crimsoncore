/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.items;

import java.util.logging.Logger;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.api.item.ItemAPI;
import com.crimsonrpg.api.item.block.BlockMarble;
import com.crimsonrpg.api.item.item.ItemBanHammer;
import com.crimsonrpg.api.item.item.ItemClanCharter;

/**
 * Crimsonitems plugin
 */
public class CrimsonItems {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	public static final String RESOURCE_BASE = "http://resources.crimsonrpg.com/s/";

	private CrimsonRPG crimson;

	public CrimsonItems(CrimsonRPG crimson) {
		this.crimson = crimson;
	}

	public void onDisable() {
		LOGGER.info("[CrimsonItems] Plugin disabled.");
	}

	public void onEnable() {
//		ItemAPI.getInstance().setCrimsonBlockManager(new SimpleCrimsonBlockManager());
//		ItemAPI.getInstance().setCrimsonItemManager(new SimpleCrimsonItemManager());
//
//		ItemAPI.getCrimsonBlockManager().addBlock("marble", new BlockMarble(crimson));
//
//		ItemAPI.getCrimsonItemManager().addItem("ban_hammer", new ItemBanHammer(crimson));
//		ItemAPI.getCrimsonItemManager().addItem("clan_charter", new ItemClanCharter(crimson));

//		ItemStack resultCharter = new SpoutItemStack(clanCharter, 1);
//		SpoutShapedRecipe recipe = new SpoutShapedRecipe(resultCharter);
//		//Note: ABC is the bottom row, CBC is the middle row, BCB is the top row
//		recipe.shape("AB", "BA");
//		recipe.setIngredient('A', MaterialData.paper);
//		recipe.setIngredient('B', MaterialData.inkSac);
//		SpoutManager.getMaterialManager().registerSpoutRecipe(recipe);

		LOGGER.info("[CrimsonItems] Plugin enabled.");
	}

}
