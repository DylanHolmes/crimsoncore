/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.economy.commands;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.api.flag.FlagCzMoney;

/**
 * Pays another player a sum.
 */
public class CommandPay extends CrimsonCommand {
    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        //Check if the person is a player
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can't pay someone if you're not a player. Try granting perhaps?");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check if there are enough arguments
        if (args.length < 2) {
            citizen.sendError("You didn't specify something! Usage: " + cmnd.getUsage());
            return;
        }
        
        //Check if the money amount is valid
        int toPay = 0;
        try {
            toPay = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            citizen.sendError("\"" + args[1] + "\" is not a valid sum of money.");
            return;
        }
        
        //Check if the player is online
        SpoutPlayer player = (SpoutPlayer) Bukkit.getServer().getPlayer(args[0]);
        if (player == null) {
            citizen.sendError("The player \"" + args[0] + "\" is either offline or does not exist.");
            return;
        }
        
        //Check if the distance is lesser than 10
        Citizen target = CrimsonAPI.getCitizenManager().getCitizen(player);
        if (citizen.getLocation().distanceSquared(target.getLocation()) > 100) {
            citizen.sendError("You must be within 10 blocks of your target to pay them.");
            return;
        }
        
        //Check money
        FlagCzMoney wallet = citizen.getFlag(FlagCzMoney.class);
        if (wallet.getMoney() < toPay) {
            citizen.sendError("You don't have enough money to pay that sum.");
            return;
        }
        
        //Pay!
        FlagCzMoney receiver = target.getFlag(FlagCzMoney.class);
        wallet.subtract(toPay);
        receiver.add(toPay);
        citizen.sendInfo("You have paid " + target.getName() + " " + ChatColor.DARK_RED + toPay + " coins.");
        target.sendInfo("You have been paid by " + target.getName() + " with " + ChatColor.GREEN + toPay + " coins.");
    }
    
}
