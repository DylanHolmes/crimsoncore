/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.economy.commands;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.flag.FlagCzMoney;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * Exchanges gold ingots for money.
 * 
 * TODO: Banker NPC
 */
public class CommandExchange extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You may only trade gold ingame.");
            return;
        }

        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);

        //Check for an amount
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify an amount of gold to trade.", MessageLevel.ERROR);
            return;
        }

        //Check for a valid amount
        String amountGiveStr = args[0];
        int amountGive = 0;
        try {
            amountGive = Integer.parseInt(amountGiveStr);
        } catch (NumberFormatException ex) {
            citizen.sendMessage("'" + amountGiveStr + "' is not a valid number.", MessageLevel.ERROR);
            return;
        }
        //Check if the person has enough gold ingots
        PlayerInventory inventory = citizen.getBukkitEntity().getInventory();
        ItemStack[] itemStacks = inventory.getContents();
        int goldIngotCount = 0;
        for (int i = 0; i < itemStacks.length; i++) {
            if (itemStacks[i] == null) {
                continue;
            }
            if (itemStacks[i].getType() == Material.GOLD_INGOT) {
                goldIngotCount += itemStacks[i].getAmount();
            }
        }

        if (goldIngotCount < amountGive) {
            citizen.sendMessage("You don't have that many gold ingots; you only have " + goldIngotCount + ".", MessageLevel.ERROR);
            return;
        }

        //Remove the ingots
        int amountGivee = amountGive;
        for (int i = 0; i < itemStacks.length; i++) {
            if (itemStacks[i] == null) {
                continue;
            }
            if (itemStacks[i].getType() != Material.GOLD_INGOT) {
                continue;
            }

            if (amountGivee < itemStacks[i].getAmount()) {
                itemStacks[i].setAmount(itemStacks[i].getAmount() - amountGivee);
                goldIngotCount -= amountGivee;
                amountGivee = 0;
                break;

            } else if (amountGivee > itemStacks[i].getAmount()) {
                goldIngotCount -= itemStacks[i].getAmount();
                amountGivee -= itemStacks[i].getAmount();
                inventory.setItem(i, null);
            } else if (amountGivee == itemStacks[i].getAmount()) {
                inventory.setItem(i, null);
                break;
            }
        }

        //Pay
        int pay = amountGive * 100;
        citizen.getFlag(FlagCzMoney.class).add(pay);

        //Notify
        citizen.sendMessage("You have received " + pay + " coins.", MessageLevel.INFO);
    }

}
