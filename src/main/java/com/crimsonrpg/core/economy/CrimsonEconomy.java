/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.economy;

import com.crimsonrpg.api.flag.FlagCzMoney;
import java.util.HashMap;
import java.util.logging.Logger;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.economy.commands.CommandExchange;
import com.crimsonrpg.core.economy.commands.CommandGimme;
import com.crimsonrpg.core.economy.commands.CommandPay;
import com.crimsonrpg.core.economy.commands.CommandWallet;
import com.crimsonrpg.flaggables.api.Flaggables;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Crimson Economy main class.
 */
public class CrimsonEconomy {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private CrimsonRPG crimson;

	public CrimsonEconomy(CrimsonRPG crimson) {
		this.crimson = crimson;
	}

	public void onDisable() {
		LOGGER.info("[CrimsonEconomy] Plugin disabled.");
	}

	public void onEnable() {
		//Register the money flag
		Flaggables.getFlagManager().registerFlag(FlagCzMoney.class);

		//Load commands
		CrimsonCommand.registerAll(new HashMap<String, CrimsonCommand>() {
			{
				put("exchange", new CommandExchange());
				put("gimme", new CommandGimme());
				put("pay", new CommandPay());
				put("wallet", new CommandWallet());
			}

		}, crimson);

		LOGGER.info("[CrimsonEconomy] Plugin enabled.");
	}

}
