/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte;

import com.crimsonrpg.api.profession.profession.Profession;
import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.profession.profession.elementalist.fire.AttributeFire;

/**
 *
 * @author simplyianm
 */
public class Necrolyte extends Profession {
	public Necrolyte(CrimsonRPG core) {
		super(core);

		id(0x02);
		name("Necrolyte");
		description("Master of the dark arts, minions, and manipulation of souls.");

		attributes(
				new AttributeFire(this));
	}

}
