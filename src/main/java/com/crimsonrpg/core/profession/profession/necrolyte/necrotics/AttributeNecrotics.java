/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.necrotics;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.profession.Profession;

/**
 *
 * @author simplyianm
 */
public class AttributeNecrotics extends Attribute {
	public AttributeNecrotics(Profession profession) {
		super(profession);

		id(0x02);
		name("Necrotics");
		description("The manipulation of the dead.");
	}

}
