/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.bloodmagic;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.api.profession.effect.EffectResult;
import com.crimsonrpg.api.profession.effect.RepeatingEffect;
import com.crimsonrpg.api.profession.skill.active.EffectSelfSkill;

/**
 *
 * @author simplyianm
 */
public class SkillBloodRitual extends EffectSelfSkill {

	public SkillBloodRitual(Attribute attribute) {
		super(attribute);

		id(0x03);
		name("Blood Ritual");
		description("Sacrifice 60...25 percent (linear) of your current health. "
			+ "Regain 3...6 health (linear) every 3 seconds for 15...30 seconds (linear).");	
	
		effect(new RepeatingEffect(this) {

			@Override
			public EffectResult onApply(Citizen citizen) {
				//remove health
				return EffectResult.SUCCESS;
			}

			@Override
			public void onRemove(Citizen citizen) {
				//Do nothing
			}

			@Override
			public void onInterval(Citizen citizen) {
				int toRegain = $$("health", citizen);
				//Add health
			}

			@Override
			public int getInterval(Citizen citizen) {
				return 3;
			}

			@Override
			public int getSeconds(Citizen citizen) {
				return $$("seconds", citizen);
			}
			
		});	
	}
	
}
