/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.elementalist.fire;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.skill.active.TargettedSkill;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author simplyianm
 */
public class SkillFlare extends TargettedSkill {
	public SkillFlare(Attribute attribute) {
		super(attribute);

		id(0x01);
		name("Flare");
		description("Hurl a fireball at the opponent for 1...8 damage (linear) from up to 100 blocks away.");

		range(100);
		castTime(1.4);
	}

	@Override
	public SkillResult use(Citizen user, LivingEntity target) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
