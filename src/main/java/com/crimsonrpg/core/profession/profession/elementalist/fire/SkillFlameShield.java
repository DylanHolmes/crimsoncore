/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.elementalist.fire;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityListener;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzProfession;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.api.profession.effect.EffectResult;
import com.crimsonrpg.api.profession.skill.active.EffectSelfSkill;

/**
 *
 * @author simplyianm
 */
public class SkillFlameShield extends EffectSelfSkill {
	public SkillFlameShield(Attribute attribute) {
		super(attribute);

		id(0x02);
		name("Flame Shield");
		description("For 10...45 seconds (linear), anything that harms you is set ablaze for 2...5 seconds (linear).");

		effect(new Effect(this) {
			{

				Bukkit.getPluginManager().registerEvent(Type.ENTITY_DAMAGE, new EntityListener() {
					@Override
					public void onEntityDamage(EntityDamageEvent event) {
						Entity e = event.getEntity();
						if (!(e instanceof Player)) {
							return;
						}

						if (!(event instanceof EntityDamageByEntityEvent)) {
							return;
						}

						Entity damager = ((EntityDamageByEntityEvent) event).getDamager();
						Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((Player) e);

						if (!isAffecting(citizen)) {
							return;
						}

						int fireTicks = (int) ($("seconds1", 
							citizen.getFlag(FlagCzProfession.class).getSkillLevel(SkillFlameShield.this)) * 20.0);

						damager.setFireTicks(fireTicks);
					}

				}, Priority.Normal, getSkill().getPlugin());
			}

			@Override
			public EffectResult onApply(Citizen citizen) {
				return EffectResult.SUCCESS;
			}

			@Override
			public void update(Citizen user) {
				//Update the fire on the person
			}

			@Override
			public void onRemove(Citizen user) {
				//ADS
			}

			@Override
			public int getSeconds(Citizen citizen) {
				return $$("seconds", citizen);
			}

		});
	}

}
