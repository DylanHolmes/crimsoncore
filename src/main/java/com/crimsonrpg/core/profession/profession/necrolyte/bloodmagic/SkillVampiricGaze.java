/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.bloodmagic;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.api.profession.skill.active.TargettedSkill;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author simplyianm
 */
public class SkillVampiricGaze extends TargettedSkill {
	public SkillVampiricGaze(Attribute attribute) {
		super(attribute);

		id(0x01);
		name("Vampiric Gaze");
		description("Steal 2...7 health (linear) from your target.");
	}

	@Override
	public SkillResult use(Citizen user, LivingEntity target) {
		int damage = $$("health", user);
		target.damage(damage);
		user.getBukkitEntity().setHealth(user.getBukkitEntity().getHealth() + damage);
		return SkillResult.NORMAL;
	}

}
