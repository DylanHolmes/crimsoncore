/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.elementalist.fire;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.profession.Profession;

/**
 *
 * @author simplyianm
 */
public class AttributeFire extends Attribute {
	public AttributeFire(Profession profession) {
		super(profession);
		
		id(0x01);
		name("Fire");
		description("Master of fire.");
	}
}
