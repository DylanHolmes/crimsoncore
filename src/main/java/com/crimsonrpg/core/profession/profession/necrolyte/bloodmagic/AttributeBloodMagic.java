/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.bloodmagic;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.profession.Profession;

/**
 *
 * @author simplyianm
 */
public class AttributeBloodMagic extends Attribute {
	public AttributeBloodMagic(Profession profession) {
		super(profession);

		id(0x01);
		name("Blood Magic");
		description("The magic of the energy of blood.");


	}

}
