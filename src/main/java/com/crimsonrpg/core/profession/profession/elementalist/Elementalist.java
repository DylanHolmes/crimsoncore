/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.elementalist;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.profession.CrimsonProfessions;
import com.crimsonrpg.core.profession.profession.elementalist.fire.AttributeFire;
import com.crimsonrpg.api.profession.profession.Profession;

/**
 *
 * @author simplyianm
 */
public class Elementalist extends Profession {
	public Elementalist(CrimsonRPG core) {
		super(core);
		
		id(0x01);
		name("Elementalist");
		description("One who controls the elements.");
		
		attributes(
				new AttributeFire(this)
				);
	}
}
