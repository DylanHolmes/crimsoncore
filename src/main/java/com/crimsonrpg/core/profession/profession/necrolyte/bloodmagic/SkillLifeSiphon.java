/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.bloodmagic;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.api.profession.effect.EffectResult;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.profession.skill.active.EffectSkill;

/**
 *
 * @author simplyianm
 */
public class SkillLifeSiphon extends EffectSkill {
	public SkillLifeSiphon(Attribute attribute) {
		super(attribute);

		id(0x02);
		name("Life Siphon");
		description("Steal 1...3 health (linear) from your target every 3 seconds. Lasts 9...18 seconds (linear).");

		effect(new Effect(this) {

			@Override
			public EffectResult onApply(Citizen citizen) {
				
				return EffectResult.SUCCESS;
			}

			@Override
			public void update(Citizen citizen) {
				
			}

			@Override
			public void onRemove(Citizen citizen) {
				throw new UnsupportedOperationException("Not supported yet.");
			}
		
		});
	}

}
