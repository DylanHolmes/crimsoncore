/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.profession.necrolyte.necrotics;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.effect.SummoningEffect;
import com.crimsonrpg.api.profession.skill.active.EffectSelfSkill;

/**
 *
 * @author simplyianm
 */
public class SkillAnimateUndeadHorror extends EffectSelfSkill {
	public SkillAnimateUndeadHorror(Attribute attribute) {
		super(attribute);

		id(0x01);
		name("Animate Undead Horror");
		description("Summons a level 1...8 (linear) Horror that can last for a maximum of 20...45 seconds (linear). Requires a captured soul.");

		effect(new SummoningEffect(this) {});
	}

}
