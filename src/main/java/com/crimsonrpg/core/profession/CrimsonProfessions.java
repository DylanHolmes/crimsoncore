/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagCzCasting;
import com.crimsonrpg.api.flag.FlagCzEffects;
import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.flaggables.api.Flaggables;
import java.util.logging.Logger;

/**
 * The Professions plugin.
 */
public class CrimsonProfessions {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private CrimsonRPG core;

	public CrimsonProfessions(CrimsonRPG core) {
		this.core = core;
	}

	public void onDisable() {
	}

	public void onEnable() {
		CrimsonAPI.getInstance().setProfessionManager(new SimpleProfessionManager(core));
		Flaggables.getFlagManager().registerFlags(
				FlagCzCasting.class,
				FlagCzEffects.class);
	}

}
