/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.description;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.profession.WikiStrip;
import com.crimsonrpg.api.profession.description.Describable;
import com.crimsonrpg.api.profession.description.Description;
import com.crimsonrpg.api.profession.description.DescriptionLoader;
import com.crimsonrpg.api.profession.description.DescriptionVariable;
import com.crimsonrpg.api.profession.description.VariableGraph;
import com.crimsonrpg.core.CrimsonRPG;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.regex.Matcher;

/**
 *
 * @author simplyianm
 */
public abstract class GenericDescriptionLoader<T extends Describable> implements DescriptionLoader<T> {
	private final MatchHelper[] helpers;

	private final String name;

	protected GenericDescriptionLoader(String name, MatchHelper... helpers) {
		this.name = name;
		this.helpers = helpers;
	}

	public Description<T> loadDescription(int maxLevel, String raw) {
		String description = WikiStrip.stripWiki(raw);

		//Strip dynamic numbers
		Map<Integer, DescriptionVariable> varsInt = new TreeMap<Integer, DescriptionVariable>();

		for (MatchHelper helper : helpers) {
			Matcher matcher = helper.getPattern().matcher(description);

			while (matcher.find()) {
				String lowS = matcher.group(helper.getLow());
				String highS = matcher.group(helper.getHigh());
				String nameS = matcher.group(helper.getName());
				String graphS = matcher.group(helper.getGraph());

				double low = 0;
				double high = 0;
				String name = nameS;

				try {
					low = Double.parseDouble(lowS);
				} catch (NumberFormatException ex) {
					CrimsonRPG.LOGGER.log(Level.WARNING, "Invalid " + name + "description number '" + low + "'.");
				}

				try {
					high = Double.parseDouble(highS);
				} catch (NumberFormatException ex) {
					CrimsonRPG.LOGGER.log(Level.WARNING, "Invalid " + name + "description number '" + high + "'.");
				}

				name = nameS.toUpperCase();
				VariableGraph graph = CrimsonAPI.getProfessionManager().getGraph(graphS);
				if (graph == null) {
					CrimsonRPG.LOGGER.log(Level.WARNING, "Unknown graph type '" + graphS + "'.");
					continue;
				}

				description = description.replace(matcher.group(), "%" + name + "%");

				String realName = name;

				for (int i = 1; containsName(realName, varsInt.values()); i++) {
					realName = (new StringBuilder(name)).append(i).toString();
				}

				varsInt.put(matcher.start(), new DescriptionVariable(low, high, maxLevel, name, graph));
			}
		}

		Map<String, DescriptionVariable> vars = new HashMap<String, DescriptionVariable>();
		for (DescriptionVariable var : varsInt.values()) {
			vars.put(var.getName(), var);
		}

		return new GenericDescription<T>(description, raw, vars);
	}

	private boolean containsName(String name, Collection<DescriptionVariable> vars) {
		for (DescriptionVariable var : vars) {
			if (var.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

}
