/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.description;

import com.crimsonrpg.api.profession.effect.Effect;
import java.util.regex.Pattern;

/**
 *
 * @author simplyianm
 */
public class EffectDescriptionLoader extends GenericDescriptionLoader<Effect> {
	private static MatchHelper a = new MatchHelper(1, 2, 3, 4, Pattern.compile("(\\d+)\\.\\.\\.(\\d+) ([\\w\\s]+) \\((\\w+)\\)"));

	private static MatchHelper b = new MatchHelper(2, 3, 1, 4, Pattern.compile("([\\w]+) (\\d+)\\.\\.\\.(\\d+) \\((\\w+)\\)"));

	public EffectDescriptionLoader() {
		//We're using the #...# ____ (__) and ____ #...# (____) ; might change.
		super("Effect", a, b);
	}

}
