package com.crimsonrpg.core.profession.description;

import java.util.regex.Pattern;

/**
 * Helper class to figure out what matches to use.
 */
public class MatchHelper {
	private final int low, high, name, graph;

	private final Pattern pattern;

	public MatchHelper(int low, int high, int name, int graph, Pattern pattern) {
		this.low = low;
		this.high = high;
		this.name = name;
		this.graph = graph;
		this.pattern = pattern;
	}

	public int getGraph() {
		return graph;
	}

	public int getHigh() {
		return high;
	}

	public int getLow() {
		return low;
	}

	public int getName() {
		return name;
	}

	public Pattern getPattern() {
		return pattern;
	}

}