/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.description;

import java.util.regex.Pattern;

import com.crimsonrpg.api.profession.skill.Skill;

/**
 *
 * @author simplyianm
 */
public class SkillDescriptionLoader extends GenericDescriptionLoader<Skill> {
	private static MatchHelper a = new MatchHelper(1, 2, 3, 4, Pattern.compile("(\\d+)\\.\\.\\.(\\d+) ([\\w\\s]+) \\((\\w+)\\)"));

	private static MatchHelper b = new MatchHelper(2, 3, 1, 4, Pattern.compile("([\\w]+) (\\d+)\\.\\.\\.(\\d+) \\((\\w+)\\)"));

	public SkillDescriptionLoader() {
		super("Skill", a, b);
	}

}
