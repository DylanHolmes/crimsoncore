/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession.description;

import com.crimsonrpg.api.profession.description.Describable;
import com.crimsonrpg.api.profession.description.Description;
import com.crimsonrpg.api.profession.description.DescriptionVariable;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.ChatColor;

/**
 *
 * @author simplyianm
 */
public class GenericDescription<T extends Describable> implements Description<T> {
	private final String description;

	private final String rawDescription;

	private final Map<String, DescriptionVariable> vars;

	public GenericDescription(String description, String rawDescription, Map<String, DescriptionVariable> vars) {
		this.description = description;
		this.rawDescription = rawDescription;
		this.vars = vars;
	}

	public String getDescription(int level) {
		return getDescription(level, null);
	}

	public String getDescription(int level, String color) {
		String desc = description;
		for (Entry<String, DescriptionVariable> var : vars.entrySet()) {
			double val = var.getValue().getValue(level);
			if (val != Math.floor(val)) {
				desc = desc.replace("%" + var.getKey() + "%", (color == null ? "" : color)
						+ Double.toString(val) + (color == null ? "" : ChatColor.WHITE)
						+ ' ' + var.getValue().getName().toLowerCase());
			} else {
				desc = desc.replace("%" + var.getKey() + "%", (color == null ? "" : color)
						+ Integer.toString(var.getValue().getValueInt(level))
						+ (color == null ? "" : ChatColor.WHITE) + ' ' + var.getValue().getName().toLowerCase());
			}
		}
		return desc;
	}

	public double getVar(String name, int level) {
		DescriptionVariable var = vars.get(name.toUpperCase());
		return (var != null) ? var.getValue(level) : -1;
	}

	public int getVarInt(String name, int level) {
		DescriptionVariable var = vars.get(name.toUpperCase());
		return (var != null) ? var.getValueInt(level) : -1;
	}

	public String getRawDescription() {
		return rawDescription;
	}

	public Map<String, DescriptionVariable> getVars() {
		return vars;
	}

}
