
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.profession;

import com.crimsonrpg.api.profession.ProfessionManager;
import com.crimsonrpg.api.profession.description.DescriptionLoader;
import com.crimsonrpg.api.profession.description.VariableGraph;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.profession.effect.Effect;
import java.util.HashMap;
import java.util.Map;

import com.crimsonrpg.api.profession.profession.Profession;
import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.profession.description.EffectDescriptionLoader;
import com.crimsonrpg.core.profession.description.SkillDescriptionLoader;
import org.bukkit.Bukkit;

/**
 * The Profession manager. Manages professions.
 */
public class SimpleProfessionManager implements ProfessionManager {
	private Map<String, Profession> professions = new HashMap<String, Profession>();

	private Map<String, Effect> effects = new HashMap<String, Effect>();

	private DescriptionLoader<Skill> sdl = new SkillDescriptionLoader();

	private DescriptionLoader<Effect> edl = new EffectDescriptionLoader();

	private static Map<String, VariableGraph> graphs = new HashMap<String, VariableGraph>();

	private CrimsonRPG core;

	public SimpleProfessionManager(CrimsonRPG core) {
		this.core = core;
		registerGraph("LINEAR", new VariableGraph() {
			@Override
			public double getValue(double percent, double low, double high) {
				return low + ((high - low) * percent);
			}

		});

		Bukkit.getScheduler().scheduleSyncRepeatingTask(core, new Runnable() {
			public void run() {
				for (Effect effect : effects.values()) {
					effect.update();
				}
			}

		}, 0L, 20L);
	}

	public void registerGraph(String name, VariableGraph graph) {
		graphs.put(name.toUpperCase(), graph);
	}

	public VariableGraph getGraph(String name) {
		return graphs.get(name.toUpperCase());
	}

	@Override
	public Profession getProfession(String name) {
		return professions.get(name.toLowerCase());
	}

	@Override
	public void registerProfession(Profession profession) {
		professions.put(profession.getName(), profession);
	}

	@Override
	public void registerProfessions(Profession... professions) {
		for (Profession profession : professions) {
			registerProfession(profession);
		}
	}

	public DescriptionLoader<Skill> getSkillDescriptionLoader() {
		return sdl;
	}

	public DescriptionLoader<Effect> getEffectDescriptionLoader() {
		return edl;
	}

	public void registerEffect(Effect effect) {
		effects.put(effect.getId(), effect);
	}

	public Effect getEffect(String id) {
		return effects.get(id);
	}

}
