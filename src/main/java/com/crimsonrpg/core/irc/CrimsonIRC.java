/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.irc;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.api.irc.ConnectionManager;
import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The CrimsonIRC plugin.
 */
public class CrimsonIRC extends JavaPlugin {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private static final ConnectionManager connectionManager = new SimpleConnectionManager();

	private CrimsonRPG core;

	public CrimsonIRC(CrimsonRPG core) {
		this.core = core;
	}

	public void onDisable() {
		LOGGER.info("[CrimsonIRC] Plugin disabled.");
	}

	public void onEnable() {
		LOGGER.info("[CrimsonIRC] Plugin enabled.");
	}

	public static ConnectionManager getConnectionManager() {
		return connectionManager;
	}

}
