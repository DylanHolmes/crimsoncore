/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.listener;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagCzClan;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityListener;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author ianschool
 */
public class CrimsonClansEntityListener extends EntityListener {

    @Override
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Entity damagee = event.getEntity();
        if (!(damagee instanceof Player)) {
            return;
        }
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        Entity damager = e.getDamager();
        if (!(damager instanceof Player)) {
            return;
        }

        Citizen target = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) damagee);
        Citizen meanie = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) damager);
        if (meanie.getFlag(FlagCzClan.class).getClan().equals(target.getFlag(FlagCzClan.class).getClan())) {
            event.setCancelled(true);
            return;
        }
    }
}
