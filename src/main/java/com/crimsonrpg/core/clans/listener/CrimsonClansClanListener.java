/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.listener;

import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.event.clan.ClanListener;
import com.crimsonrpg.api.event.clan.ClanRenameEvent;

/**
 * The core clan listener.
 */
public class CrimsonClansClanListener extends ClanListener {

    @Override
    public void onClanRename(ClanRenameEvent event) {
        if (event.isCancelled()) return;
        ClanUtils.sendClanMessage(event.getClan(), "The clan has been renamed to '" + event.getNewName() +"'.");
    }
    
}
