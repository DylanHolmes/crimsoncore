/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.listener;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnBase;
import com.crimsonrpg.api.flag.FlagCzClan;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 *
 * @author simplyianm
 */
public class CrimsonClansPlayerListener extends PlayerListener {
	@Override
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Citizen c = CrimsonAPI.getCitizenManager().getCitizen(event.getPlayer());
		Clan cn = c.getFlag(FlagCzClan.class).getClan();
		if (cn != null) {
			Location cnSpawn = cn.getFlag(FlagCnBase.class).getSpawn();
			if (cnSpawn != null) {
				event.setRespawnLocation(cnSpawn);
			}
		}
	}

}
