/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.listener;

import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.inventory.ItemStack;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.inventory.SpoutItemStack;
import org.getspout.spoutapi.material.CustomItem;

/**
 *
 * @author simplyianm
 */
public class CCPlayerListener extends PlayerListener {

    @Override
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        ItemStack hand = event.getPlayer().getItemInHand();
        if (hand == null || !(hand instanceof SpoutItemStack)) {
            return;
        }
        SpoutItemStack stack = (SpoutItemStack) hand;
        if (!stack.isCustomItem()) {
            return;
        }
    }
}
