/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.listener;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagPtClan;
import com.crimsonrpg.api.plot.Role;
import com.crimsonrpg.api.event.plot.PlotCheckRoleEvent;
import com.crimsonrpg.api.event.plot.PlotListener;

/**
 *
 * @author simplyianm
 */
public class CCPlotListener extends PlotListener {

    @Override
    public void onPlotCheckRole(PlotCheckRoleEvent event) {
        //Check for a clan
        Clan clan = event.getPlot().getFlag(FlagPtClan.class).getClan();
        Role role = event.getRole();
        if (clan != null) {
            switch(clan.getFlag(FlagCnRanks.class).getRank(event.getCitizen())) {
                case RECRUIT:
                    role = Role.MEMBER;
                    
                case MEMBER:
                    role = Role.BUILDER;
                    
                case VETERAN:
                    role = Role.MOD;
                    
                case OFFICER:
                    role = Role.ADMIN;
                    
                case FOUNDER:
                    role = Role.OWNER;
            }
            event.setRole(role);
        }
    }
    
}
