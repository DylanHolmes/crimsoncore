/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Privilege;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 *
 * @author Dylan
 */
public class CommandClanInspect extends CrimsonCommand {
    
    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You may only inspect people ingame.");
            return;
        }
        

        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        //Check if there is a name input
        if (args.length < 1) {
            citizen.sendError("You didn't input a name!");
            return;
        }
        
        String toInspectName = args[0];
        Player inspectedPlayer = Bukkit.getServer().getPlayer(toInspectName);
        
        if (inspectedPlayer == null) {
            citizen.sendMessage("That player is not online.", MessageLevel.ERROR);
            return;
        }
        
        Citizen inspected = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) inspectedPlayer);
        
        //Get the real name if the player is online
        toInspectName = inspectedPlayer.getName();
        
        //Check if the player is in a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not in a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check the rank
        Rank rank = clan.getFlag(FlagCnRanks.class).getRank(inspected);
        if (rank == null) {
            citizen.sendMessage("That player is not part of the clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to inspect players in the clan.
        if (!rank.hasPrivilege(Privilege.RECRUIT_INSPECT)) {
            citizen.sendMessage("You aren't allowed to inspect members in this clan.", MessageLevel.ERROR);
            return;
        }
        
        //Send the inspected's info
        citizen.sendMessage("The player's role is: " + rank.niceName(), MessageLevel.INFO);
    }
    
}
