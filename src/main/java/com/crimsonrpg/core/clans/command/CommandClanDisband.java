/*
 * This code is released under the
 * Creative Commons by-nc license.
 * Don't claim this as your own and don't
 * use this code for profit without the permission
 * of the code author.
 */
package com.crimsonrpg.core.clans.command;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.event.clan.ClanDisbandEvent.ClanDisbandReason;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.util.CrimsonCommand;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * Disbands a clan.
 */
public class CommandClanDisband extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Disbanded the clan 'Console Robots'. :3");
            return;
        }
        
        //Get the citizen
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Get the clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        
        //Check if the player is in a clan
        if (clan == null) {
            citizen.sendError("You are not part of a clan.");
            return;
        }
        
        //Check clan privileges (wow, this is one long line)
        if (!clan.getFlag(FlagCnRanks.class).getRank(citizen).hasPrivilege(FlagCnRanks.Privilege.FOUNDER_DISBAND)) {
            citizen.sendError("You're not allowed to disband this clan.");
            return;
        }
        
        //TODO: are you sure message
        
        //Disband the clan
        CrimsonAPI.getClanManager().disbandClan(clan, ClanDisbandReason.COMMAND);
        List<Citizen> memberList = clan.getFlag(FlagCnRanks.class).getOnlineMembers();
        for (Citizen c : memberList) {
            c.getFlag(FlagCzClan.class).reset();
        }
        
        //Message all members
        ClanUtils.sendClanMessage(clan, "Your clan '" + clan.getFlag(FlagCnName.class).getName() + "' has been disbanded.");
        
    }
    
}
