/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.event.clan.ClanRenameEvent;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.util.MD5Helper;
import com.crimsonrpg.util.StringStuff;
import org.bukkit.Bukkit;

/**
 *
 * @author simplyianm
 */
public class CommandClanRename extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can't promote players from the console.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify a new name.", MessageLevel.ERROR);
            return;
        }
        
        //Check for a legit name
        String clanName = StringStuff.join(args);
        if (clanName.length() > 20) {
            citizen.sendMessage("Clan names must be under 20 characters.", MessageLevel.ERROR);
        }
        
        //Check for alphanumeric, apostrophes, and spaces
        if (!(clanName.matches("[a-zA-Z0-9\\'\\s]+"))) {
            citizen.sendMessage("Plot names can only consist of letters, numbers, apostrophes, and spaces.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);
        if (!rank.hasPrivilege(FlagCnRanks.Privilege.FOUNDER_RENAME)) {
            citizen.sendMessage("You aren't allowed to rename your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Generate a hash
        String hash = MD5Helper.md5(clanName);
        
        //Check if the hashes are equivalent to another or if another clan has the same name
        for (Clan cn : CrimsonAPI.getClanManager().getClans()) {
            if (cn.getId().equals(hash) || cn.getFlag(FlagCnName.class).getName().equals(clanName)) {
                citizen.sendMessage("That clan name is already taken.");
                return;
            }
        }
        
        //Pass an event
        ClanRenameEvent event = new ClanRenameEvent(clan, citizen, hash, clanName);
        Bukkit.getPluginManager().callEvent(event);
        
        //Check for a cancelled event
        if (event.isCancelled()) {
            return;
        }
        
        //Rename the clan
        clan.getFlag(FlagCnName.class).setName(event.getNewName());
        CrimsonAPI.getClanManager().changeId(clan, event.getHash());
    }
}
