/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Transfers founder rights of the clan to another person.
 */
public class CommandClanTransfer extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can't promote players from the console.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify a player.", MessageLevel.ERROR);
            return;
        }
        
        String playerName = args[0];
        
        //Check if the player is online
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            citizen.sendMessage("That player is not online.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);
        if (!rank.hasPrivilege(FlagCnRanks.Privilege.FOUNDER_TRANSFER)) {
            citizen.sendMessage("You aren't a founder of this clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the target is part of the clan
        Citizen target = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) player);
        Rank tRank = ranks.getRank(target);
        if (tRank == null) {
            citizen.sendMessage("That player is not part of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is eligible for founder
        if (tRank.ordinal() < Rank.OFFICER.ordinal()) {
            citizen.sendMessage("You can only transfer founder rights to another officer.", MessageLevel.ERROR);
            return;
        }
        
        //Transfer founder rights
        ranks.addMember(target, Rank.FOUNDER);
        ranks.addMember(citizen, Rank.OFFICER);
        
        ClanUtils.sendClanMessage(clan, citizen.getName() + " has handed founder rights to " + target.getName() + ".");
    }
}