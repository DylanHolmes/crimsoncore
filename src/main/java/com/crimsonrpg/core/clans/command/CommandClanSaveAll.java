/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;

import com.crimsonrpg.core.clans.CrimsonClans;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 *
 * @author simplyianm
 */
public class CommandClanSaveAll extends CrimsonCommand {
    private CrimsonClans plugin;

    public CommandClanSaveAll(CrimsonClans plugin) {
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender.hasPermission("crimson.rank.superadmin"))) {
            sender.sendMessage(ChatColor.DARK_RED + "You're not allowed to use this command.");
            return;
        }
        
        sender.sendMessage("Saving clans...");
        plugin.save();
        sender.sendMessage("Clans saved.");
    }
}
