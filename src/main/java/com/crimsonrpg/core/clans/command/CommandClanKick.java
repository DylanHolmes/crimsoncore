/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.util.StringStuff;
import org.bukkit.Bukkit;

/**
 * Kicks a person from the clan.
 */
public class CommandClanKick extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You are forever alone.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify a player.", MessageLevel.ERROR);
            return;
        }
        
        String playerName = args[0];
        
        //Check if the player is online
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            citizen.sendMessage("That player is not online.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);
        if (!rank.hasPrivilege(FlagCnRanks.Privilege.OFFICER_KICK)) {
            citizen.sendMessage("You aren't allowed to kick members of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the target is part of the clan
        Citizen target = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) player);
        Rank tRank = ranks.getRank(target);
        if (tRank == null) {
            citizen.sendMessage("That player is not part of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick the target
        if (rank.ordinal() <= tRank.ordinal()) {
            citizen.sendMessage("You aren't allowed to kick members with a rank equal to or higher than your own.", MessageLevel.ERROR);
            return;
        }
        
        //Check for a kick message (like IRC!)
        String message = "";
        if (args.length > 1) {
            message = StringStuff.join(StringStuff.remFirstArg(args)).trim();
        }
        
        //Kick the target (after all these checks)
        ranks.kick(target);
        target.sendMessage("You have been kicked from the clan. " + (message.length() > 0 ? message : ""), MessageLevel.CLAN);
        ClanUtils.sendClanMessage(clan, target.getName() + " has been kicked from the clan.");
    }
    
}
