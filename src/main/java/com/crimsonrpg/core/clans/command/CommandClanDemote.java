/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Demotes a player within the clan.
 */
public class CommandClanDemote extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can't demote players from the console.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify a player.", MessageLevel.ERROR);
            return;
        }
        
        String playerName = args[0];
        
        //Check if the player is online
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            citizen.sendMessage("That player is not online.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to demote
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);
        if (!rank.hasPrivilege(FlagCnRanks.Privilege.OFFICER_DEMOTE)) {
            citizen.sendMessage("You aren't allowed to demote members of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the target is part of the clan
        Citizen target = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) player);
        Rank tRank = ranks.getRank(target);
        if (tRank == null) {
            citizen.sendMessage("That player is not part of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to promote the target
        if (rank.ordinal() <= tRank.ordinal()) {
            citizen.sendMessage("You aren't allowed to demote members with a rank equal to or higher than your rank.", MessageLevel.ERROR);
            return;
        }
        //Promote the target.
        Rank newRank = ranks.demote(target);
        
        //Check if the demotion was successful
        if (newRank == null || newRank.equals(tRank)) {
            citizen.sendMessage("That player could not be promoted any higher.", MessageLevel.ERROR);
            return;
        }
        
        ClanUtils.sendClanMessage(clan, target.getName() + " has been demoted to a " + newRank.niceName() + ".");
    }
    
}
