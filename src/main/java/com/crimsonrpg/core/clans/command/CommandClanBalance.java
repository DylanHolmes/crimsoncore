/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnMoney;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.util.CrimsonCommand;

/**
 *
 * @author simplyianm
 */
public class CommandClanBalance extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use this game ingame.");
            return;
        }

        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);

        //Check if the money amount is valid
        String amountStr = args[0];
        int amount = 0;
        try {
            amount = Integer.parseInt(amountStr);
        } catch (NumberFormatException e) {
            citizen.sendMessage("That is an invalid amount of money.", MessageLevel.ERROR);
            return;
        }

        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();

        //Check if the clan exists
        if (clan == null) {
            citizen.sendMessage("You aren't in a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check privileges
        if (!clan.getFlag(FlagCnRanks.class).getRank(citizen).hasPrivilege(FlagCnRanks.Privilege.RECRUIT_BALANCE)) {
            citizen.sendMessage("You aren't allowed to view the balance of the clan.", MessageLevel.ERROR);
            return;
        }
        
        //Notify
        citizen.sendMessage("The balance of the clan is " + clan.getFlag(FlagCnMoney.class).getMoney() + " coins.", MessageLevel.INFO);
    }

}
