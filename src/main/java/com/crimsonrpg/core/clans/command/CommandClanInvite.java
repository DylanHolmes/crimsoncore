/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.flag.FlagCzInviteClan;
import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.util.StringStuff;

/**
 * Invites a player to the clan.
 */
public class CommandClanInvite extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Nobody will join your clan, so I'm not sending the invite.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length < 1) {
            citizen.sendMessage("You didn't specify a player.", MessageLevel.ERROR);
            return;
        }
        
        String playerName = args[0];
        
        //Check if the player is online
        Player player = Bukkit.getPlayer(playerName);
        if (player == null) {
            citizen.sendMessage("That player is not online.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);
        if (!rank.hasPrivilege(FlagCnRanks.Privilege.MEMBER_INVITE)) {
            citizen.sendMessage("You aren't allowed to kick members of your clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the target is part of a clan
        Citizen target = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) player);
        Clan tClan = target.getFlag(FlagCzClan.class).getClan();
        if (tClan != null) {
            citizen.sendMessage("That player is already part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check for an invite message (like IRC!)
        String message = "";
        if (args.length > 1) {
            message = StringStuff.join(StringStuff.remFirstArg(args)).trim();
        }
        
        //Invite the target and dispatch messages.
        target.getFlag(FlagCzInviteClan.class).setClan(clan);
        
        citizen.sendMessage("An invite has been dispatched to " + target.getName() + ".", MessageLevel.CLAN);
        target.sendMessage("You have been invited to the clan '" + clan.getFlag(FlagCnName.class).getName() + "'. To accept, type '/clanaccept'.", MessageLevel.CLAN);
        if (message.length() > 0) {
            target.sendMessage("Message: " + message, MessageLevel.CLAN);
        }
    }
}
