/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnMoney;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.flag.FlagCzMoney;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * Deposits money into the clan.
 * 
 * TODO: make it only work around a clan banker
 */
public class CommandClanDeposit extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use this game ingame.");
            return;
        }

        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);

        //Check if the money amount is valid
        String amountStr = args[0];
        int amount = 0;
        try {
            amount = Integer.parseInt(amountStr);
        } catch (NumberFormatException e) {
            citizen.sendMessage("That is an invalid amount of money.", MessageLevel.ERROR);
            return;
        }

        //Check if the player has enough money
        FlagCzMoney wallet = citizen.getFlag(FlagCzMoney.class);
        int money = wallet.getMoney();
        if (money < amount) {
            citizen.sendMessage("You don't have that much money.", MessageLevel.ERROR);
            return;
        }

        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();

        //Check if the clan exists
        if (clan == null) {
            citizen.sendMessage("You aren't in a clan.", MessageLevel.ERROR);
            return;
        }

        //Check privileges
        if (!clan.getFlag(FlagCnRanks.class).getRank(citizen).hasPrivilege(FlagCnRanks.Privilege.RECRUIT_DEPOSIT)) {
            citizen.sendMessage("You're not allowed to deposit money into the clan.", MessageLevel.ERROR);
            return;
        }

        //Add to the clan's coffers
        FlagCnMoney clanMoneyFlag = clan.getFlag(FlagCnMoney.class);
        clanMoneyFlag.add(amount);
        wallet.subtract(amount);

        //Notify
        citizen.sendMessage("You have deposited " + amount + " coins in your account.", MessageLevel.INFO);
    }

}
