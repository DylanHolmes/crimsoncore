/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.ChatColor;

/**
 * Presents some clan information.
 */
public class CommandClanInfo extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Disbanded the clan 'Console Robots'. :3");
            return;
        }
        
        //Get the citizen
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Get the clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        
        //Check if the player is in a clan
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check clan privileges
        if (!clan.getFlag(FlagCnRanks.class).getRank(citizen).hasPrivilege(FlagCnRanks.Privilege.RECRUIT_INFO)) {
            citizen.sendMessage("You're not allowed to view your clan's information.", MessageLevel.ERROR);
            return;
        }
        
        //Print out standard clan statistics
        citizen.sendMessage("=== " + clan.getFlag(FlagCnName.class).getName() + " ===", MessageLevel.CLAN);
        
        //Rank stuff
        FlagCnRanks ranksFlag = clan.getFlag(FlagCnRanks.class);
        citizen.sendMessage("Members online: " + ranksFlag.getOnlineMemberCount() + "/" + ranksFlag.getMemberCount());
        citizen.sendMessage("Founder: " + ChatColor.WHITE + ranksFlag.getFounderName(), MessageLevel.INFO);
        citizen.sendMessage("Officers: " + "TODO");
    }
    
}
