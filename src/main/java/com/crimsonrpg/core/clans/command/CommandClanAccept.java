/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.flag.FlagCzInviteClan;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Accepts the last invite dispatched.
 */
public class CommandClanAccept extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("It's not even possible to invite you, so why would you try to accept an invite?");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check if the citizen has been invited
        Clan clan = citizen.getFlag(FlagCzInviteClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You have not been invited to a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Add the citizen to the clan.
        clan.getFlag(FlagCnRanks.class).addMember(citizen, FlagCnRanks.Rank.RECRUIT);
        citizen.getFlag(FlagCzClan.class).setClan(clan);
        
        ClanUtils.sendClanMessage(clan, citizen.getName() + " has joined the clan.");
    }
    
}
