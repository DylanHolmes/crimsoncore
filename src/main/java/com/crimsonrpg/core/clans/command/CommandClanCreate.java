/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.event.clan.ClanCreateEvent.ClanCreateReason;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.flag.FlagCzMoney;
import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.util.MD5Helper;

/**
 * Creates a clan.
 */
public class CommandClanCreate extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You're hilarious. Who would join the clan of a person who can't even join the game?");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check if the player is already part of a clan
        Clan current = citizen.getFlag(FlagCzClan.class).getClan();
        if (current != null) {
            citizen.sendMessage("You are already part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check for a clan name
        if (args.length < 1) {
            citizen.sendError("You must specify a name for your clan.");
            return;
        }
        
        //TODO: Guild charter item (for now do money)
        
        //Build the clan name
        StringBuilder out = new StringBuilder();
        out.append(args[0]);
        if (args.length >= 2) {
            for (int i = 1; i < args.length; i++) {
                out.append(' ').append(args[i]);
            }
        }
        String clanName = out.toString();
        
        //Check name length
        if (clanName.length() > 20) {
            citizen.sendMessage("That clan name is too long; please choose a different one.", MessageLevel.ERROR);
            return;
        }
        
        //Check for alphanumeric, apostrophes, and spaces
        if (!(clanName.matches("[a-zA-Z0-9\\'\\s]+"))) {
            citizen.sendMessage("Plot names can only consist of letters, numbers, apostrophes, and spaces.", MessageLevel.ERROR);
            return;
        }
        
        //money
        FlagCzMoney moneyAttribute = citizen.getFlag(FlagCzMoney.class);
        int clanCost = 500;
        
        if (moneyAttribute.getMoney() < clanCost) {
            citizen.sendError("Not enough money, You need " + clanCost + " coins!");
            return;
        }
        moneyAttribute.subtract(clanCost);
        
        //Get the clan
        String id = MD5Helper.md5(clanName);
        Clan clan = CrimsonAPI.getClanManager().createClan(id, ClanCreateReason.COMMAND);
        
        //Set the clan's name
        FlagCnName clanNameFlag = clan.getFlag(FlagCnName.class);
        clanNameFlag.setName(clanName);
        
        //Add the player to the clan
        clan.getFlag(FlagCnRanks.class).addMember(citizen, Rank.FOUNDER);
        citizen.getFlag(FlagCzClan.class).setClan(clan);
        
        citizen.sendMessage("You have founded the clan '" + clanName + "'.", MessageLevel.CLAN);
    }
    
}
