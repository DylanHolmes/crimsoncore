/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCzClan;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.crimsonrpg.util.CrimsonCommand;
import com.crimsonrpg.util.StringStuff;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * Clan chat
 */
public class CommandClanChat extends CrimsonCommand {
    
    @Override
    public void execute(CommandSender cs, Command cmnd, String string, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage("Impossible.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) cs);
        
        if (args.length < 1) {
            citizen.sendMessage("You didn't put in an message.", MessageLevel.ERROR);
            return;
        }
        
        Clan tClan = citizen.getFlag(FlagCzClan.class).getClan();
        
        if(tClan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        String message = StringStuff.join(args);
        ClanUtils.sendClanMessage(tClan, "[Clan] " + citizen.getName() + ": " + message);
    }   
}
