/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans.command;


import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanUtils;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCnRanks.Rank;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Kicks a person from the clan.
 */
public class CommandClanLeave extends CrimsonCommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You are forever alone.");
            return;
        }
        
        Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);
        
        //Check arg
        if (args.length > 0) {
            citizen.sendMessage("You don't need anything else.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is part of a clan
        Clan clan = citizen.getFlag(FlagCzClan.class).getClan();
        if (clan == null) {
            citizen.sendMessage("You are not part of a clan.", MessageLevel.ERROR);
            return;
        }
        
        //Check if the citizen is allowed to kick
        FlagCnRanks ranks = clan.getFlag(FlagCnRanks.class);
        Rank rank = ranks.getRank(citizen);    
        
        //Kick the target (after all these checks)
        ranks.kick(citizen);
        citizen.sendMessage("You have left your clan.", MessageLevel.CLAN);
        ClanUtils.sendClanMessage(clan, citizen.getName() + " has left the clan.");
    }
    
}
