/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.flaggables.api.GenericFlaggable;

/**
 * Represents a clan.
 */
public class SimpleClan extends GenericFlaggable implements Clan {
	public SimpleClan(String id) {
		super(id);
	}

	@Override
	public String toString() {
		return "SimpleClan{" + "id=" + getId() + '}';
	}

}
