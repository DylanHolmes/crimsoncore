/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.clan.ClanManager;
import com.crimsonrpg.api.event.clan.ClanCreateEvent.ClanCreateReason;
import com.crimsonrpg.api.event.clan.ClanDisbandEvent.ClanDisbandReason;
import com.crimsonrpg.flaggables.api.Flag;
import com.crimsonrpg.flaggables.api.Flaggables;

public class SimpleClanManager implements ClanManager {
	private Map<String, Clan> clans = new HashMap<String, Clan>();

	public Clan getClan(String id) {
		//First check
		Clan clan = clans.get(id);
		if (clan != null) {
			return clan;
		}

		//Get the clan folder
		File clanFolder = new File("./plugins/CrimsonClans/clans/");
		clanFolder.mkdirs();

		//Check if the clan exists
		File clanFile = new File(clanFolder.getPath() + File.separator + id + ".yml");
		if (!clanFile.exists()) {
			return null; //Fundamental difference between this and citizens
		}

		//Create the clan
		clan = new SimpleClan(id);

		ConfigurationSection clanSection = YamlConfiguration.loadConfiguration(clanFile);
		List<Flag> flagList = Flaggables.getFlagManager().makeFlagList(clanSection);
		clan.addFlags(flagList);

		//Put in the map
		clans.put(id, clan);

		return clan;
	}

	public Clan createClan(String id) {
		return createClan(id, ClanCreateReason.UNKNOWN);
	}

	public Clan createClan(String id, ClanCreateReason reason) {
		Clan clan = new SimpleClan(id);
		clans.put(id, clan);
		return clan;
	}

	public void disbandClan(Clan clan) {
		disbandClan(clan, ClanDisbandReason.UNKNOWN);
	}

	public void disbandClan(Clan clan, ClanDisbandReason reason) {
		//Destroy the clan file
		File clanFile = new File("./plugins/CrimsonClans/clans/" + clan.getId() + ".yml");
		if (clanFile.exists()) {
			clanFile.delete();
		}

		clans.remove(clan.getId());
	}

	public void changeId(Clan clan, String id) {
		clans.put(id, clan);
		clan.setId(id);
	}

	public List<Clan> getClans() {
		return new ArrayList(clans.values());
	}

}