/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.clans;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.plugin.PluginManager;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagCnBase;
import com.crimsonrpg.api.flag.FlagCnMoney;
import com.crimsonrpg.api.flag.FlagCnName;
import com.crimsonrpg.api.flag.FlagCnRanks;
import com.crimsonrpg.api.flag.FlagCzClan;
import com.crimsonrpg.api.flag.FlagCzInviteClan;
import com.crimsonrpg.api.flag.FlagPtClan;
import com.crimsonrpg.core.clans.command.CommandClanAccept;
import com.crimsonrpg.core.clans.command.CommandClanBalance;
import com.crimsonrpg.core.clans.command.CommandClanChat;
import com.crimsonrpg.core.clans.command.CommandClanCreate;
import com.crimsonrpg.core.clans.command.CommandClanDemote;
import com.crimsonrpg.core.clans.command.CommandClanDeposit;
import com.crimsonrpg.core.clans.command.CommandClanDisband;
import com.crimsonrpg.core.clans.command.CommandClanInfo;
import com.crimsonrpg.core.clans.command.CommandClanInspect;
import com.crimsonrpg.core.clans.command.CommandClanInvite;
import com.crimsonrpg.core.clans.command.CommandClanKick;
import com.crimsonrpg.core.clans.command.CommandClanLeave;
import com.crimsonrpg.core.clans.command.CommandClanPlot;
import com.crimsonrpg.core.clans.command.CommandClanPromote;
import com.crimsonrpg.core.clans.command.CommandClanRename;
import com.crimsonrpg.core.clans.command.CommandClanSaveAll;
import com.crimsonrpg.core.clans.command.CommandClanSetBase;
import com.crimsonrpg.core.clans.command.CommandClanTransfer;
import com.crimsonrpg.core.clans.command.CommandClanWithdraw;
import com.crimsonrpg.core.clans.listener.CCPlotListener;
import com.crimsonrpg.core.clans.listener.CrimsonClansClanListener;
import com.crimsonrpg.core.clans.listener.CrimsonClansEntityListener;
import com.crimsonrpg.core.clans.listener.CrimsonClansPlayerListener;
import com.crimsonrpg.flaggables.api.Flaggables;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * The CrimsonClans main plugin file.
 * 
 * TODO: Janitor to clean up unneeded clans
 */
public class CrimsonClans {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	public static final String RESOURCE_BASE = "http://resources.crimsonrpg.com/s/";

	private CrimsonRPG crimson;

	public CrimsonClans(CrimsonRPG crimson) {
		this.crimson = crimson;
	}

	public void onDisable() {
		save();
		LOGGER.info("[CrimsonClans] Plugin disabled.");
	}

	public void onEnable() {
		//Initialize the clan API
		CrimsonAPI.getInstance().setClanManager(new SimpleClanManager());

		//Register events
		PluginManager pm = Bukkit.getPluginManager();

		CrimsonClansEntityListener entityListener = new CrimsonClansEntityListener();
		pm.registerEvent(Event.Type.ENTITY_DAMAGE, entityListener, Priority.Normal, crimson);

		CrimsonClansPlayerListener playerListener = new CrimsonClansPlayerListener();
		pm.registerEvent(Event.Type.PLAYER_RESPAWN, playerListener, Priority.Normal, crimson);

		CCPlotListener plotListener = new CCPlotListener();
		pm.registerEvent(Type.CUSTOM_EVENT, plotListener, Priority.Normal, crimson);

		CrimsonClansClanListener clanListener = new CrimsonClansClanListener();
		pm.registerEvent(Type.CUSTOM_EVENT, clanListener, Priority.Monitor, crimson);

		//Register citizen flags
		Flaggables.getFlagManager().registerFlags(
				//Clan flags
				FlagCnBase.class,
				FlagCnMoney.class,
				FlagCnName.class,
				FlagCnRanks.class,
				//Citizen flags
				FlagCzClan.class,
				FlagCzInviteClan.class,
				//Plot flags
				FlagPtClan.class);

		//Schedule the clan saver
		Bukkit.getScheduler().scheduleSyncRepeatingTask(crimson, new Runnable() {
			public void run() {
				save();
			}

		}, 6000L, 6000L); //Approximately 5 minutes

		//Register commands
		CrimsonCommand.registerAll(new HashMap<String, CrimsonCommand>() {
			{

				put("clanaccept", new CommandClanAccept());
				put("clanbalance", new CommandClanBalance());
				put("clanchat", new CommandClanChat());
				put("clancreate", new CommandClanCreate());
				put("clandemote", new CommandClanDemote());
				put("clandeposit", new CommandClanDeposit());
				put("clandisband", new CommandClanDisband());
				put("claninfo", new CommandClanInfo());
				put("claninspect", new CommandClanInspect());
				put("claninvite", new CommandClanInvite());
				put("clankick", new CommandClanKick());
				put("clanleave", new CommandClanLeave());
				put("clanplot", new CommandClanPlot());
				put("clanpromote", new CommandClanPromote());
				put("clanrename", new CommandClanRename());
				put("clansaveall", new CommandClanSaveAll(CrimsonClans.this));
				put("clansetbase", new CommandClanSetBase());
				put("clantransfer", new CommandClanTransfer());
				put("clanwithdraw", new CommandClanWithdraw());
			}

		}, crimson);

		//Enabled
		LOGGER.info("[CrimsonClans] Plugin enabled.");
	}

	public void save() {
		CrimsonClans.LOGGER.info("[CrimsonClans] Beginning saving of all clans...");

		File clansFolder = new File("./plugins/CrimsonClans/clans/");
		clansFolder.mkdirs();

		for (Clan clan : CrimsonAPI.getClanManager().getClans()) {
			String fileName = clan.getId();

			File thisClan = new File(clansFolder.getPath() + File.separator + fileName + ".yml");

			try {
				thisClan.createNewFile();
			} catch (IOException ex) {
				CrimsonClans.LOGGER.severe("[Crimson] Could not make the clan file for clan id " + fileName + ".");
			}

			//Save stats
			FileConfiguration clanSection = YamlConfiguration.loadConfiguration(thisClan);
			Flaggables.getFlagManager().storeFlagList(clan.getFlags(), clanSection);

			try {
				clanSection.save(thisClan);
			} catch (IOException ex) {
				CrimsonClans.LOGGER.severe("[Crimson] Could not save the clan file for clan id " + fileName + ".");
			}
		}

		CrimsonClans.LOGGER.info("[CrimsonClans] All clans have been written to files.");
	}

}
