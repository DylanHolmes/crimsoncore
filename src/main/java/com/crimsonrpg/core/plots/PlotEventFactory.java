/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.plot.Plot;
import com.crimsonrpg.api.plot.Role;
import com.crimsonrpg.api.event.plot.PlotCheckRoleEvent;
import com.crimsonrpg.api.flag.FlagPtAdminified;
import com.crimsonrpg.api.flag.FlagPtRoles;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

/**
 *
 * @author simplyianm
 */
public class PlotEventFactory {
	private static void callEvent(Event event) {
		Bukkit.getServer().getPluginManager().callEvent(event);
	}

	public static PlotCheckRoleEvent callPlotCheckRoleEvent(Plot plot, Citizen citizen) {
		Role role = plot.getFlag(FlagPtRoles.class).getRole(citizen);
		if (role.getRank() <= Role.DEFAULT.getRank() && plot.getFlag(FlagPtAdminified.class).isAdminified()) {
			role = Role.OUTSIDER;
		}
		PlotCheckRoleEvent event = new PlotCheckRoleEvent(plot, citizen, role);
		callEvent(event);
		return event;
	}

}
