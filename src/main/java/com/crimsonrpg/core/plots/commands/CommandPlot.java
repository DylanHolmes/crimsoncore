/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots.commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.crimsonrpg.util.CrimsonCommand;

/**
 * Represents an executor for the plot command.
 * 
 * TODO: Add all plot commands into this!
 */
public class CommandPlot extends CrimsonCommand {
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (args.length == 0) {
            Bukkit.dispatchCommand(sender, "plothelp");
            return;
        }

        String function = args[0];
        String command = "";
        
        if (args.length > 1) {
            String[] newArgs = new String[args.length - 1];
            System.arraycopy(args, 1, newArgs, 0, args.length - 1);
            StringBuilder joiner = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                joiner.append(args[i]).append(" ");
            }
            command = joiner.toString().trim();
        }
        
        if (function.equalsIgnoreCase("accept")) {
            Bukkit.dispatchCommand(sender, "plotaccept " + command);
            
        } else if (function.equalsIgnoreCase("adminify")) {
            Bukkit.dispatchCommand(sender, "plotadminify " + command);

        } else if (function.equalsIgnoreCase("buy")) {
            Bukkit.dispatchCommand(sender, "plotbuy " + command);

        } else if (function.equalsIgnoreCase("coowner")) {
            Bukkit.dispatchCommand(sender, "plotcoowner " + command);

        } else if (function.equalsIgnoreCase("create")) {
            Bukkit.dispatchCommand(sender, "plotcreate " + command);

        } else if (function.equalsIgnoreCase("demote")) {
            Bukkit.dispatchCommand(sender, "plotdemote " + command);
            
        } else if (function.equalsIgnoreCase("deposit")) {
            Bukkit.dispatchCommand(sender, "plotdeposit " + command);
            
        } else if (function.equalsIgnoreCase("disband")) {
            Bukkit.dispatchCommand(sender, "plotdisband " + command);
        
        } else if (function.equalsIgnoreCase("downgrade")) {
            Bukkit.dispatchCommand(sender, "plotdowngrade " + command);

        } else if (function.equalsIgnoreCase("expand")) {
            Bukkit.dispatchCommand(sender, "plotexpand " + command);

        } else if (function.equalsIgnoreCase("fire")) {
            Bukkit.dispatchCommand(sender, "plotfire " + command);
            
        } else if (function.equalsIgnoreCase("help")) {
            Bukkit.dispatchCommand(sender, "plothelp " + command);

        } else if (function.equalsIgnoreCase("hirebanker")) {
            Bukkit.dispatchCommand(sender, "plothirebanker " + command);

        } else if (function.equalsIgnoreCase("hireguard")) {
            Bukkit.dispatchCommand(sender, "plothireguard " + command);

        } else if (function.equalsIgnoreCase("hirevendor")) {
            Bukkit.dispatchCommand(sender, "plothirevendor " + command);
            
        } else if (function.equalsIgnoreCase("info")) {
            Bukkit.dispatchCommand(sender, "plotinfo " + command);

        } else if (function.equalsIgnoreCase("inspect")) {
            Bukkit.dispatchCommand(sender, "plotinspect " + command);
        
        } else if (function.equalsIgnoreCase("invite")) {
            Bukkit.dispatchCommand(sender, "plotinvite " + command);

        } else if (function.equalsIgnoreCase("kick")) {
            Bukkit.dispatchCommand(sender, "plotkick " + command);

        } else if (function.equalsIgnoreCase("leave")) {
            Bukkit.dispatchCommand(sender, "plotleave " + command);

        } else if (function.equalsIgnoreCase("promote")) {
            Bukkit.dispatchCommand(sender, "plotpromote " + command);

        } else if (function.equalsIgnoreCase("rename")) {
            Bukkit.dispatchCommand(sender, "plotrename " + command);

        } else if (function.equalsIgnoreCase("sell")) {
            Bukkit.dispatchCommand(sender, "plotsell " + command);
            
        } else if (function.equalsIgnoreCase("shrink")) {
            Bukkit.dispatchCommand(sender, "plotshrink " + command);

        } else if (function.equalsIgnoreCase("size")) {
            Bukkit.dispatchCommand(sender, "plotsize " + command);

        } else if (function.equalsIgnoreCase("unsell")) {
            Bukkit.dispatchCommand(sender, "plotunsell " + command);
            
        } else if (function.equalsIgnoreCase("upgrade")) {
            Bukkit.dispatchCommand(sender, "plotupgrade " + command);
            
        } else if (function.equalsIgnoreCase("withdraw")) {
            Bukkit.dispatchCommand(sender, "plotwithdraw " + command);
            
        } else if (function.equalsIgnoreCase("?")) {
            Bukkit.dispatchCommand(sender, "plothelp " + command);
            
        } else {
            Bukkit.dispatchCommand(sender, "plothelp " + command);
            
        }
    }
}
