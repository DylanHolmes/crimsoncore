/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots.commands;


import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.crimsonrpg.core.plots.CrimsonPlots;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Loads all plots.
 */
public class CommandPlotLoadAll extends CrimsonCommand {
    private CrimsonPlots plugin;

    public CommandPlotLoadAll(CrimsonPlots plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs.hasPermission("crimson.rank.superadmin"))) {
            cs.sendMessage(ChatColor.DARK_RED + "You're not allowed to use this command.");
            return;
        }
        
        cs.sendMessage("Loading plots...");
        plugin.load();
        cs.sendMessage("Plots loaded.");
    }
}
