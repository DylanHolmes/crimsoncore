/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots.commands;

import com.crimsonrpg.core.plots.CrimsonPlots;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/**
 * Saves all plots.
 */
public class CommandPlotSaveAll extends CrimsonCommand {
    private CrimsonPlots plugin;

    public CommandPlotSaveAll(CrimsonPlots plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs.hasPermission("crimson.rank.superadmin"))) {
            cs.sendMessage(ChatColor.DARK_RED + "You're not allowed to use this command.");
            return;
        }
        
        cs.sendMessage("Saving plots...");
        plugin.save();
        cs.sendMessage("Plots saved.");
    }
    
}
