/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots;

import com.crimsonrpg.flaggables.api.Flag;
import com.crimsonrpg.flaggables.api.FlaggableLoader;
import com.crimsonrpg.flaggables.api.GenericFlaggableManager;
import com.crimsonrpg.api.plot.Plot;
import com.crimsonrpg.api.plot.PlotManager;
import java.util.List;

/**
 * Represents a simple plot manager.
 */
public class SimplePlotManager extends GenericFlaggableManager<Plot> implements PlotManager {

    public SimplePlotManager() {
        super(new FlaggableLoader<Plot>() {

            public Plot create(String id, List<Flag> flags) {
                if (flags == null) {
                    return new SimplePlot(id);
                }
                Plot plot = new SimplePlot(id);
                plot.addFlags(flags);
                return plot;
            }
        });
    }
}