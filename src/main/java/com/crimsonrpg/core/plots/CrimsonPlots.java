/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.plots;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.api.flag.FlagCzLastPlotRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.block.BlockListener;
import org.bukkit.event.entity.EntityListener;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.plugin.PluginManager;

import com.crimsonrpg.flaggables.api.Flaggables;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.flag.FlagPtAdminified;
import com.crimsonrpg.api.flag.FlagPtFounder;
import com.crimsonrpg.api.flag.FlagPtFunds;
import com.crimsonrpg.api.flag.FlagPtLevel;
import com.crimsonrpg.api.flag.FlagPtLocation;
import com.crimsonrpg.api.flag.FlagPtNPCs;
import com.crimsonrpg.api.flag.FlagPtName;
import com.crimsonrpg.api.flag.FlagPtRoles;
import com.crimsonrpg.api.flag.FlagPtSalePrice;
import com.crimsonrpg.api.flag.FlagPtSize;
import com.crimsonrpg.core.plots.commands.CommandPlot;
import com.crimsonrpg.core.plots.commands.CommandPlotAccept;
import com.crimsonrpg.core.plots.commands.CommandPlotAdminify;
import com.crimsonrpg.core.plots.commands.CommandPlotBuy;
import com.crimsonrpg.core.plots.commands.CommandPlotCoowner;
import com.crimsonrpg.core.plots.commands.CommandPlotCreate;
import com.crimsonrpg.core.plots.commands.CommandPlotDemote;
import com.crimsonrpg.core.plots.commands.CommandPlotDeposit;
import com.crimsonrpg.core.plots.commands.CommandPlotDisband;
import com.crimsonrpg.core.plots.commands.CommandPlotDowngrade;
import com.crimsonrpg.core.plots.commands.CommandPlotExpand;
import com.crimsonrpg.core.plots.commands.CommandPlotHelp;
import com.crimsonrpg.core.plots.commands.CommandPlotHireBanker;
import com.crimsonrpg.core.plots.commands.CommandPlotInfo;
import com.crimsonrpg.core.plots.commands.CommandPlotInspect;
import com.crimsonrpg.core.plots.commands.CommandPlotInvite;
import com.crimsonrpg.core.plots.commands.CommandPlotKick;
import com.crimsonrpg.core.plots.commands.CommandPlotLeave;
import com.crimsonrpg.core.plots.commands.CommandPlotLoadAll;
import com.crimsonrpg.core.plots.commands.CommandPlotPVP;
import com.crimsonrpg.core.plots.commands.CommandPlotPromote;
import com.crimsonrpg.core.plots.commands.CommandPlotRename;
import com.crimsonrpg.core.plots.commands.CommandPlotSaveAll;
import com.crimsonrpg.core.plots.commands.CommandPlotSell;
import com.crimsonrpg.core.plots.commands.CommandPlotShrink;
import com.crimsonrpg.core.plots.commands.CommandPlotSize;
import com.crimsonrpg.core.plots.commands.CommandPlotUnsell;
import com.crimsonrpg.core.plots.commands.CommandPlotUpgrade;
import com.crimsonrpg.core.plots.commands.CommandPlotWithdraw;
import com.crimsonrpg.core.plots.listener.CPBlockListener;
import com.crimsonrpg.core.plots.listener.CPEntityListener;
import com.crimsonrpg.core.plots.listener.CPPlayerListener;
import com.crimsonrpg.util.CrimsonCommand;
import java.io.File;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * The Crimson Plots plugin.
 */
public class CrimsonPlots {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private BlockListener blockListener;

	private EntityListener entityListener;

	private PlayerListener playerListener;

	private FileConfiguration config;

	private CrimsonRPG crimson;

	public CrimsonPlots(CrimsonRPG crimson) {
		this.crimson = crimson;
	}

	public void onDisable() {
		save();
		LOGGER.info("[CrimsonPlots] Plugin disabled.");
	}

	public void onEnable() {
		//Load config
		File configDir = new File("./plugins/CrimsonRPG/");
		configDir.mkdirs();
		File configFile = new File(configDir.getPath() + File.separator + "plots.yml");
		try {
			configFile.createNewFile();
		} catch (IOException ex) {
			Logger.getLogger(CrimsonPlots.class.getName()).log(Level.SEVERE, null, ex);
		}

		config = YamlConfiguration.loadConfiguration(configFile);

		//Plot load
		CrimsonAPI.getInstance().setPlotManager(new SimplePlotManager());

		//Tax collector
		crimson.getServer().getScheduler().scheduleSyncRepeatingTask(crimson, new Taxman(this), 0L, 18000L); //18000L = 15 minutes approx

		//Create the scheduled saver
		crimson.getServer().getScheduler().scheduleSyncRepeatingTask(crimson, new Runnable() {
			public void run() {
				save();
			}

		}, 6000L, 6000L); //6000L = 5 minutes approx

		//Register events
		blockListener = new CPBlockListener();
		entityListener = new CPEntityListener();
		playerListener = new CPPlayerListener();

		PluginManager pluginManager = crimson.getServer().getPluginManager();
		pluginManager.registerEvent(Type.BLOCK_BREAK, blockListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.BLOCK_BURN, blockListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.BLOCK_PLACE, blockListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.BLOCK_IGNITE, blockListener, Priority.Normal, crimson);

		pluginManager.registerEvent(Type.CREATURE_SPAWN, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.ENDERMAN_PICKUP, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.ENDERMAN_PLACE, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.ENTITY_DAMAGE, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.ENTITY_EXPLODE, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.ENTITY_INTERACT, entityListener, Priority.Normal, crimson);

		pluginManager.registerEvent(Type.PLAYER_MOVE, playerListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.PLAYER_INTERACT, playerListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.PLAYER_RESPAWN, playerListener, Priority.Normal, crimson);

		//Register flags
		Flaggables.getFlagManager().registerFlags(FlagPtAdminified.class,
				FlagPtFounder.class, FlagPtFunds.class,
				FlagPtLevel.class, FlagPtLocation.class,
				FlagPtNPCs.class, FlagPtName.class,
				FlagPtRoles.class, FlagPtSize.class,
				FlagPtSalePrice.class, FlagCzLastPlotRequest.class);

		//Register commands
		HashMap<String, CrimsonCommand> commands = new HashMap<String, CrimsonCommand>();
		commands.put("plot", new CommandPlot());
		commands.put("plotaccept", new CommandPlotAccept());
		commands.put("plotadminify", new CommandPlotAdminify());
		commands.put("plotbuy", new CommandPlotBuy());
		commands.put("plotcreate", new CommandPlotCreate());
		commands.put("plotcoowner", new CommandPlotCoowner());
		commands.put("plotdemote", new CommandPlotDemote());
		commands.put("plotdeposit", new CommandPlotDeposit());
		commands.put("plotdisband", new CommandPlotDisband());
		commands.put("plotdowngrade", new CommandPlotDowngrade());
		commands.put("plotexpand", new CommandPlotExpand());
//        commands.put("plotfire", new CommandPlotFire());
		commands.put("plothelp", new CommandPlotHelp());
		commands.put("plothirebanker", new CommandPlotHireBanker());
//        commands.put("plothireguard", new CommandPlotHireGuard());
//        commands.put("plothirevendor", new CommandPlotHireVendor());
		commands.put("plotinfo", new CommandPlotInfo());
		commands.put("plotinspect", new CommandPlotInspect());
		commands.put("plotinvite", new CommandPlotInvite());
		commands.put("plotkick", new CommandPlotKick());
		commands.put("plotleave", new CommandPlotLeave());
		commands.put("plotloadall", new CommandPlotLoadAll(this));
		commands.put("plotpromote", new CommandPlotPromote());
		commands.put("plotpvp", new CommandPlotPVP());
		commands.put("plotrename", new CommandPlotRename());
		commands.put("plotsaveall", new CommandPlotSaveAll(this));
		commands.put("plotshrink", new CommandPlotShrink());
		commands.put("plotsell", new CommandPlotSell());
		commands.put("plotsize", new CommandPlotSize());
		commands.put("plotunsell", new CommandPlotUnsell());
		commands.put("plotupgrade", new CommandPlotUpgrade());
		commands.put("plotwithdraw", new CommandPlotWithdraw());

		//Register the commands
		CrimsonCommand.registerAll(commands, crimson);

		//Load
		load();

		LOGGER.info("[CrimsonPlots] Plugin enabled.");
	}

	public void load() {
		//Load the plots
		Configuration config = getConfig();
		if (config.isSet("plots")) {
			CrimsonAPI.getPlotManager().load(config.getConfigurationSection("plots"));
		}
	}

	public void save() {
		CrimsonAPI.getPlotManager().save(getConfig().createSection("plots"));
		saveConfig();
	}

	public FileConfiguration getConfig() {
		return config;
	}

	public void saveConfig() {
		try {
			config.save(new File("./plugins/CrimsonRPG/plots.yml"));
		} catch (IOException ex) {
			Logger.getLogger(CrimsonPlots.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
