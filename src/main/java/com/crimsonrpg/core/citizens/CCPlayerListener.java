/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.citizens;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;

/**
 * Represents the CrimsonCitizens player listener.
 */
public class CCPlayerListener extends PlayerListener {
	private CrimsonCitizens cc;

	public CCPlayerListener(CrimsonCitizens cc) {
		this.cc = cc;
	}

	@Override
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) event.getPlayer());
		citizen.sendMessage(ChatColor.GOLD + "=== Welcome to " + ChatColor.RED + "CrimsonRPG" + ChatColor.GOLD + "! ===");
		citizen.sendMessage(ChatColor.GRAY + "If you have not read the wiki, please do so at " + ChatColor.AQUA + "http://wiki.crimsonrpg.com/" + ChatColor.GRAY + ".");

		//Changelog
		citizen.sendMessage(ChatColor.GOLD + "=== Changelog ===");
		citizen.sendMessage(ChatColor.YELLOW + "0.0.1" + ChatColor.WHITE + " - Initial release.");

		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(cc.getCrimson(), new Runnable() {
			public void run() {
				if (!(citizen.getSpoutEntity().isSpoutCraftEnabled())) {
					citizen.sendError("It seems you don't have Spoutcraft; we strongly advise that you get it. You can get it at " + ChatColor.GOLD + "http://spout.in/jar" + ChatColor.RED + ".");
				}
			}

		}, 200L);
	}

	@Override
	public void onPlayerQuit(PlayerQuitEvent event) {
		cc.save();
		CrimsonAPI.getCitizenManager().unloadCitizen((SpoutPlayer) event.getPlayer());
	}

	@Override
	public void onPlayerKick(PlayerKickEvent event) {
		cc.save();
		CrimsonAPI.getCitizenManager().unloadCitizen((SpoutPlayer) event.getPlayer());
	}

	@Override
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (event.getAction().equals(Action.RIGHT_CLICK_AIR)
				&& event.getMaterial().equals(Material.APPLE) || event.getMaterial().equals(Material.BREAD) || event.getMaterial().equals(Material.COOKED_BEEF) || event.getMaterial().equals(Material.COOKED_CHICKEN)
				|| event.getMaterial().equals(Material.COOKED_FISH) || event.getMaterial().equals(Material.GRILLED_PORK) || event.getMaterial().equals(Material.GOLDEN_APPLE) || event.getMaterial().equals(Material.PORK) || event.getMaterial().equals(Material.RAW_BEEF) || event.getMaterial().equals(Material.RAW_CHICKEN) || event.getMaterial().equals(Material.RAW_FISH) || event.getMaterial().equals(Material.MUSHROOM_SOUP)) {
			if (player.getFoodLevel() == 20) {
				return;
			}
			SpoutPlayer sPlayer = (SpoutPlayer) player;
			SpoutManager.getSoundManager().playCustomMusic(cc.getCrimson(), sPlayer, "http://resources.crimsonrpg.com/s/audio/eatingNoise.ogg", true);
		}
	}

}
