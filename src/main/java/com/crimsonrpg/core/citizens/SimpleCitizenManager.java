/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.citizens;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.citizen.CitizenManager;
import com.crimsonrpg.flaggables.api.Flag;
import com.crimsonrpg.flaggables.api.Flaggables;

/**
 * Represents a simple citizen manager.
 */
public class SimpleCitizenManager implements CitizenManager {
    private Map<Player, Citizen> citizenList = new HashMap<Player, Citizen>();
    
    /**
     * Loads a Citizen to the world.
     * 
     * @param player
     * @return 
     */
    private Citizen loadCitizen(Player player) {
        
        //Create the citizen object
        SimpleCitizen citizen = new SimpleCitizen(player.getName());
        citizen.setBukkitEntity(player);
        citizenList.put(player, citizen);
        
        
        File citizenFolder = new File("./plugins/CrimsonCitizens/citizens/" + player.getName() + File.separator);
        citizenFolder.mkdirs();
        
        File attributeFile = new File(citizenFolder.getPath() + File.separator + "attributes.yml");
        
        //Check if the citizen is new; if so, wait for a save
        if (!attributeFile.exists()) {
            return citizen;
        }
        
        ConfigurationSection flagsSection = YamlConfiguration.loadConfiguration(attributeFile);
        List<Flag> flags = Flaggables.getFlagManager().makeFlagList(flagsSection);
        for (Flag flag : flags) citizen.setFlag(flag);
        
        return citizen;
    }

    public List<Citizen> getCitizenList() {
        return new ArrayList(citizenList.values());
    }
    
    public Citizen getCitizen(Player player) {
        Citizen citizen = citizenList.get(player);
        return (citizen == null) ? loadCitizen(player) : citizen;
    }

    public void unloadCitizen(Player player) {
        citizenList.remove(player);
    }
}
