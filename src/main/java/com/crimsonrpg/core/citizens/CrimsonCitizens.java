/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.citizens;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.core.citizens.command.CommandCitizenSaveAll;
import com.crimsonrpg.flaggables.api.Flaggables;
import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.plugin.PluginManager;

/**
 *
 * @author simplyianm
 */
public class CrimsonCitizens {
	private CrimsonRPG crimson;

	public CrimsonCitizens(CrimsonRPG crimson) {
		this.crimson = crimson;
	}

	public void onDisable() {
		save();
	}
	
	public void onEnable() {
		CrimsonAPI.getInstance().setCitizenManager(new SimpleCitizenManager());

		crimson.getCommand("citizensaveall").setExecutor(new CommandCitizenSaveAll(this));

		PlayerListener playerListener = new CCPlayerListener(this);

		PluginManager pluginManager = Bukkit.getServer().getPluginManager();
		pluginManager.registerEvent(Type.PLAYER_JOIN, playerListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.PLAYER_QUIT, playerListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.PLAYER_INTERACT, playerListener, Priority.Normal, crimson);

		//Create the scheduled saver
		crimson.getServer().getScheduler().scheduleSyncRepeatingTask(crimson, new Runnable() {
			public void run() {
				save();
			}

		}, 6000L, 6000L); //6000L = 5 minutes approx

		CrimsonRPG.LOGGER.info("[CrimsonCitizens] Plugin enabled.");
	}

	public void save() {
		File citizenFolder = new File("./plugins/CrimsonCitizens/citizens/");
		citizenFolder.mkdirs();

		for (Citizen citizen : CrimsonAPI.getCitizenManager().getCitizenList()) {
			String folderName = citizen.getName();

			File crimsonCitizen = new File(citizenFolder.getPath() + File.separator + folderName + File.separator);
			crimsonCitizen.mkdirs();

			File attributeFile = new File(crimsonCitizen.getPath() + File.separator + "attributes.yml");
			try {
				attributeFile.createNewFile();
			} catch (IOException ex) {
				CrimsonRPG.LOGGER.severe("[CrimsonCitizens] Could not create a citizen file for '" + citizen.getName() + "'.");
			}

			FileConfiguration flagSection = YamlConfiguration.loadConfiguration(attributeFile);
			Flaggables.getFlagManager().storeFlagList(citizen.getFlags(), flagSection);

			try {
				flagSection.save(attributeFile);
			} catch (IOException ex) {
				CrimsonRPG.LOGGER.severe("[CrimsonCitizens] Could not save the attributes file for '" + citizen.getName() + "'.");
			}
		}
	}

	public CrimsonRPG getCrimson() {
		return crimson;
	}

}
