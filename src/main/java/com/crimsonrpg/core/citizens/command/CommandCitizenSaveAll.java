/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.citizens.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.crimsonrpg.core.citizens.CrimsonCitizens;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Saves all citizens.
 */
public class CommandCitizenSaveAll extends CrimsonCommand {
	private CrimsonCitizens plugin;

	public CommandCitizenSaveAll(CrimsonCitizens plugin) {
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
		if (!(sender.hasPermission("crimson.rank.superadmin"))) {
			sender.sendMessage(ChatColor.DARK_RED + "You're not allowed to use this command.");
			return;
		}

		sender.sendMessage("Saving citizens...");
		plugin.save();
		sender.sendMessage("Citizens saved.");
	}

}
