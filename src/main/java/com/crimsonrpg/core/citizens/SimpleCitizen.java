/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.citizens;

import com.crimsonrpg.api.citizen.MessageLevel;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.GenericFlaggable;

/**
 * Represents a citizen of the world.
 * This is a wrapper for a Player.
 */
public class SimpleCitizen extends GenericFlaggable implements Citizen {

    private Player player = null;

    public SimpleCitizen(String name) {
        super(name);
    }

    public String getName() {
        return player.getName();
    }

    public Location getLocation() {
        return player.getLocation();
    }

    public boolean hasPermission(String permission) {
        return player.hasPermission(permission);
    }

    public void sendMessage(String string) {
        player.sendMessage(string);
    }

    public void sendMessage(String message, MessageLevel level) {
        player.sendMessage(level.getColor().toString() + message);
    }

    public Player getBukkitEntity() {
        return player;
    }

    public SpoutPlayer getSpoutEntity() {
        if (player instanceof SpoutPlayer) {
            return (SpoutPlayer) player;
        } else {
            throw new IllegalArgumentException("Spout isn't enabled at the moment!");
        }
    }

    protected void setBukkitEntity(Player player) {
        if (this.player == null) {
            this.player = player;
        }
    }

    //DEPRECATED
    public void sendError(String string) {
        player.sendMessage(ChatColor.DARK_RED + string);
    }

    public void sendInfo(String string) {
        player.sendMessage(ChatColor.YELLOW + string);
    }

    public void sendPlotMessage(String string) {
        player.sendMessage(ChatColor.BLUE + string);
    }

    public void sendPrivateMessage(String string) {
        player.sendMessage(ChatColor.LIGHT_PURPLE + string);
    }
}
