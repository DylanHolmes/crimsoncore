/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.chat.commands;

import com.crimsonrpg.core.CrimsonRPG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.flag.FlagCzLastMessaged;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.util.CrimsonCommand;
import org.getspout.spoutapi.SpoutManager;

/**
 * Sends a private message to someone.
 */
public class CommandMessage extends CrimsonCommand {
	private CrimsonRPG plugin;

	public CommandMessage(CrimsonRPG instance) {
		plugin = instance;
	}

	@Override
	public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("Private messaging can only be used ingame.");
			return;
		}

		Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);

		if (args.length < 2) {
			citizen.sendError(cmnd.getUsage());
			return;
		}

		String receiverStr = args[0];

		Player receiverPlayer = Bukkit.getServer().getPlayer(receiverStr);

		//Check if the player is online
		if (receiverPlayer == null) {
			citizen.sendMessage(ChatColor.RED + "That player is either not online does not exist!");
			return;
		}

		//Checks if the player is sending a message to his self.
		if (citizen.getName().equals(receiverPlayer.getName())) {
			citizen.sendError("You have created a paradox, and your message has not been sent.");
			return;
		}

		Citizen receiver = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) receiverPlayer);

		SpoutPlayer player = (SpoutPlayer) receiver;
		//Form the message string
		StringBuilder messageBuilder = new StringBuilder();
		for (int i = 1; i < args.length; i++) {
			messageBuilder.append(args[i]).append(" ");
		}
		String message = messageBuilder.toString().trim();

		//Send the message
		citizen.sendPrivateMessage("[PM] To " + ChatColor.YELLOW + receiver.getName() + ChatColor.LIGHT_PURPLE + ": " + message);
		receiver.sendPrivateMessage("[PM] From " + ChatColor.YELLOW + citizen.getName() + ChatColor.LIGHT_PURPLE + ": " + message);
		receiver.getFlag(FlagCzLastMessaged.class).setLastMessaged(citizen.getName());
		citizen.getFlag(FlagCzLastMessaged.class).setLastMessaged(receiver.getName());

		//Courtesy of camaroow
		SpoutManager.getSoundManager().playCustomMusic(plugin, citizen.getSpoutEntity(), "http://resources.crimsonrpg.com/s/audio/privateMessage.ogg", true);
		SpoutManager.getSoundManager().playCustomMusic(plugin, receiver.getSpoutEntity(), "http://resources.crimsonrpg.com/s/audio/privateMessage.ogg", true);
	}

}
