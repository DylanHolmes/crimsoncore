/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.chat.commands;

import com.crimsonrpg.core.CrimsonRPG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.core.chat.CrimsonChat;
import com.crimsonrpg.api.flag.FlagCzLastMessaged;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * Replies.
 */
public class CommandReply extends CrimsonCommand {
	private CrimsonRPG plugin;

	public CommandReply(CrimsonRPG instance) {
		plugin = instance;
	}

	@Override
	public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("Private messaging can only be used ingame.");
			return;
		}

		Citizen citizen = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) sender);

		if (args.length < 1) {
			citizen.sendError(cmnd.getUsage());
			return;
		}

		//Check if the citizen has anyone to reply to
		String receiverStr = citizen.getFlag(FlagCzLastMessaged.class).getLastMessaged();
		if (receiverStr == null || "".equals(receiverStr)) {
			citizen.sendError("You haven't messaged anyone in the past since logging on!");
			return;
		}

		//Checks if the player is sending a message to his self.
		if (citizen.getName().equals(receiverStr)) {
			citizen.sendError("You have created a paradox so your message has not been sent.");
			return;
		}

		//Checks if the player is online
		Player receiverPlayer = Bukkit.getServer().getPlayer(receiverStr);
		if (receiverPlayer == null) {
			citizen.sendError("That player is no longer online.");
			return;
		}

		Citizen receiver = CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) receiverPlayer);

		//Form the message string
		StringBuilder messageBuilder = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			messageBuilder.append(args[i]).append(" ");
		}
		String message = messageBuilder.toString().trim();

		//Send the message
		citizen.sendPrivateMessage("[PM] To " + ChatColor.YELLOW + receiver.getName() + ChatColor.LIGHT_PURPLE + ": " + message);
		receiver.sendPrivateMessage("[PM] From " + ChatColor.YELLOW + citizen.getName() + ChatColor.LIGHT_PURPLE + ": " + message);
		receiver.getFlag(FlagCzLastMessaged.class).setLastMessaged(citizen.getName());
		citizen.getFlag(FlagCzLastMessaged.class).setLastMessaged(receiver.getName());

		//PM sound courtesy of Camaroow
		SpoutManager.getSoundManager().playCustomMusic(plugin, citizen.getSpoutEntity(), CrimsonChat.RESOURCE_BASE + "s/privateMessage.ogg", true);
		SpoutManager.getSoundManager().playCustomMusic(plugin, receiver.getSpoutEntity(), CrimsonChat.RESOURCE_BASE + "s/privateMessage.ogg", true);
	}

}
