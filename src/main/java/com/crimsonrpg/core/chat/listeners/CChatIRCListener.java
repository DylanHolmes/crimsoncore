/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.chat.listeners;

import com.crimsonrpg.core.chat.CrimsonChat;
import com.crimsonrpg.api.event.irc.ChannelMessageEvent;
import com.crimsonrpg.api.event.irc.IRCListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * The CrimsonChat IRC listener.
 */
public class CChatIRCListener extends IRCListener {
	private CrimsonChat plugin;

	public CChatIRCListener(CrimsonChat plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onChannelMessage(ChannelMessageEvent event) {
		String message = event.getMessage();
		if (message.isEmpty()) {
			return;
		}
		if (message.charAt(0) != '.') {
			return;
		}
		if (message.length() < 2) {
			return;
		}
		int indexOf = message.indexOf(' ');

		String command = (indexOf > 0) ? message.substring(1, indexOf).trim() : message.substring(1);
		String trueMsg = (indexOf > 0) ? message.substring(indexOf + 1, message.length()).trim() : "";

		//Handle console commands
		if (event.getChannel().getName().equals("#crimcons")) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), (command + " " + trueMsg).trim());
			return;
		}

		//TODO: add some commands
		if (command.equals("say")) {
			for (Player player : Bukkit.getOnlinePlayers()) {
				player.sendMessage("[IRC] " + event.getSender() + ": " + trueMsg);
			}

			CrimsonChat.LOGGER.info("[IRC] " + event.getSender() + ": " + trueMsg);

			event.getConnection().getChannel(event.getSender()).sendMessage("Your message has been sent to the game.");
		}
	}

}
