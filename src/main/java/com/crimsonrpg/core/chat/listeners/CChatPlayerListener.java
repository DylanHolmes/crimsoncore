/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.chat.listeners;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerListener;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.core.chat.CrimsonChat;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;

/**
 * Removes the default chat.
 */
public class CChatPlayerListener extends PlayerListener {
    private CrimsonChat plugin;

    public CChatPlayerListener(CrimsonChat plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public void onPlayerChat(PlayerChatEvent event) {
        event.setCancelled(true);
        Citizen speaker = CrimsonAPI.getCitizenManager().getCitizen(event.getPlayer());
        Player[] players = Bukkit.getServer().getOnlinePlayers();
        String message = "[Global] " + ChatColor.BLUE + speaker.getName() + ": " + ChatColor.WHITE + event.getMessage();
        for (Player player : players) {
            player.sendMessage(message);
        }
        
        CrimsonChat.LOGGER.info("[CrimsonChat] " + ChatColor.stripColor(message));
        plugin.getConnection().getChannel("#crimsonrpg").sendMessage(ChatColor.stripColor(message));
    }
}
