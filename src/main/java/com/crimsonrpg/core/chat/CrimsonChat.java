/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.chat;

import com.crimsonrpg.api.flag.FlagCzLastMessaged;
import com.crimsonrpg.core.CrimsonRPG;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;

import com.crimsonrpg.core.chat.commands.CommandMessage;
import com.crimsonrpg.core.chat.commands.CommandReply;
import com.crimsonrpg.core.chat.listeners.CChatIRCListener;
import com.crimsonrpg.core.chat.listeners.CChatPlayerListener;
import com.crimsonrpg.flaggables.api.Flaggables;
import com.crimsonrpg.core.irc.CrimsonIRC;
import com.crimsonrpg.api.irc.Connection;
import com.crimsonrpg.util.CrimsonCommand;

/**
 * The Crimson Chat Plugin
 */
public class CrimsonChat {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	public static final String RESOURCE_BASE = "http://resources.crimsonrpg.com/";

	private CrimsonRPG crimson;

	private Connection connection;

	public CrimsonChat(CrimsonRPG plugin) {
		this.crimson = plugin;
	}

	public void onDisable() {
		connection.disconnect();

		LOGGER.info("[CrimsonChat] Plugin disabled.");
	}

	public void onEnable() {

		Flaggables.getFlagManager().registerFlag(FlagCzLastMessaged.class);

		connection = CrimsonIRC.getConnectionManager().getConnection("irc.esper.net", "CrimsonBot");
		connection.connect();

		connection.getChannel("#crimsonrpg").join();
		connection.getChannel("#crimcons").join("peterstaplenightforty");

		Bukkit.getServer().getPluginManager().registerEvent(Type.PLAYER_CHAT, new CChatPlayerListener(this), Priority.Highest, crimson);
		Bukkit.getServer().getPluginManager().registerEvent(Type.CUSTOM_EVENT, new CChatIRCListener(this), Priority.Normal, crimson);

		Map<String, CrimsonCommand> commands = new HashMap<String, CrimsonCommand>() {
			{
				put("message", new CommandMessage(crimson));
				put("reply", new CommandReply(crimson));
			}

		};

		CrimsonCommand.registerAll(commands, crimson);

//        Thread reader = new Thread(new ConsoleReader(this), "chat-console-reader");
//        reader.start();

		LOGGER.info("[CrimsonChat] Plugin enabled.");
	}

	public Connection getConnection() {
		return connection;
	}

}
