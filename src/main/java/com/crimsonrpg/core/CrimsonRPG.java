/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core;

import com.crimsonrpg.core.admin.CrimsonAdmin;
import com.crimsonrpg.core.chat.CrimsonChat;
import com.crimsonrpg.core.citizens.CrimsonCitizens;
import com.crimsonrpg.core.clans.CrimsonClans;
import com.crimsonrpg.core.economy.CrimsonEconomy;
import com.crimsonrpg.core.irc.CrimsonIRC;
import com.crimsonrpg.core.items.CrimsonItems;
import com.crimsonrpg.core.plots.CrimsonPlots;
import com.crimsonrpg.core.profession.CrimsonProfessions;
import com.crimsonrpg.core.sin.CrimsonSin;
import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author simplyianm
 */
public class CrimsonRPG extends JavaPlugin {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private CrimsonAdmin crimsonAdmin;

	private CrimsonChat crimsonChat;

	private CrimsonCitizens crimsonCitizens;

	private CrimsonClans crimsonClans;

	private CrimsonEconomy crimsonEconomy;

	private CrimsonIRC crimsonIRC;

	private CrimsonItems crimsonItems;

	private CrimsonPlots crimsonPlots;

	private CrimsonProfessions crimsonProfessions;

	private CrimsonSin crimsonSin;

	public void onDisable() {
		crimsonAdmin.onDisable();
		crimsonChat.onDisable();
		crimsonCitizens.onDisable();
		crimsonClans.onDisable();
		crimsonEconomy.onDisable();
		crimsonIRC.onDisable();
		crimsonItems.onDisable();
		crimsonPlots.onDisable();
		crimsonProfessions.onDisable();
		crimsonSin.onDisable();
	}

	public void onEnable() {
		crimsonAdmin = new CrimsonAdmin(this);
		crimsonChat = new CrimsonChat(this);
		crimsonCitizens = new CrimsonCitizens(this);
		crimsonClans = new CrimsonClans(this);
		crimsonEconomy = new CrimsonEconomy(this);
		crimsonIRC = new CrimsonIRC(this);
		crimsonItems = new CrimsonItems(this);
		crimsonPlots = new CrimsonPlots(this);
		crimsonProfessions = new CrimsonProfessions(this);
		crimsonSin = new CrimsonSin(this);

		crimsonAdmin.onEnable();
		crimsonChat.onEnable();
		crimsonCitizens.onEnable();
		crimsonClans.onEnable();
		crimsonEconomy.onEnable();
		crimsonIRC.onEnable();
		crimsonItems.onEnable();
		crimsonPlots.onEnable();
		crimsonProfessions.onEnable();
		crimsonSin.onEnable();
	}

}
