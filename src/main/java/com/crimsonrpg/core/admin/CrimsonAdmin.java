/*1
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.core.admin;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.admin.listeners.CABlockListener;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.entity.EntityListener;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.plugin.PluginManager;

import com.crimsonrpg.core.admin.commands.CommandBring;
import com.crimsonrpg.core.admin.commands.CommandClear;
import com.crimsonrpg.core.admin.commands.CommandCustomBlock;
import com.crimsonrpg.core.admin.commands.CommandCustomItem;
import com.crimsonrpg.core.admin.commands.CommandFakeGameMode;
import com.crimsonrpg.core.admin.commands.CommandForceLoad;
import com.crimsonrpg.core.admin.commands.CommandForceSave;
import com.crimsonrpg.core.admin.commands.CommandGetPos;
import com.crimsonrpg.core.admin.commands.CommandHelp;
import com.crimsonrpg.core.admin.commands.CommandItem;
import com.crimsonrpg.core.admin.commands.CommandSetSpawn;
import com.crimsonrpg.core.admin.commands.CommandSun;
import com.crimsonrpg.core.admin.commands.CommandTeleport;
import com.crimsonrpg.core.admin.commands.CommandTeleportPosition;
import com.crimsonrpg.core.admin.listeners.CAEntityListener;
import com.crimsonrpg.core.admin.listeners.CAPlayerListener;
import com.crimsonrpg.util.CrimsonCommand;
import org.bukkit.event.block.BlockListener;
import org.getspout.spoutapi.SpoutManager;

/**
 * The CrimsonAdmin plugin.
 */
public class CrimsonAdmin {
	public static final Logger LOGGER = Logger.getLogger("Minecraft");

	private CrimsonRPG crimson;

	public CrimsonAdmin(CrimsonRPG plugin) {
		super();
		this.crimson = plugin;
	}

	public void onDisable() {
		LOGGER.info("[CrimsonAdmin] Plugin disabled.");
	}

	public void onEnable() {
		HashMap<String, CrimsonCommand> commands = new HashMap<String, CrimsonCommand>();
		commands.put("bring", new CommandBring());
		commands.put("customitem", new CommandCustomItem());
		commands.put("fakegamemode", new CommandFakeGameMode());
		commands.put("forceload", new CommandForceLoad());
//      commands.put("forcesave", new CommandForceReload(this));
		commands.put("forcesave", new CommandForceSave());
		commands.put("getpos", new CommandGetPos());
		commands.put("item", new CommandItem());
		commands.put("teleport", new CommandTeleport());
		commands.put("tpos", new CommandTeleportPosition());
		commands.put("setspawn", new CommandSetSpawn());
//        commands.put("spawn", new CommandSpawn());
		commands.put("customblock", new CommandCustomBlock());
		commands.put("clearinventory", new CommandClear());
		commands.put("sun", new CommandSun());
		commands.put("help", new CommandHelp());

		CrimsonCommand.registerAll(commands, crimson);
		EntityListener entityListener = new CAEntityListener(crimson);
		PlayerListener playerListener = new CAPlayerListener(crimson);
		BlockListener blockListener = new CABlockListener(crimson);

		//Registers the events
		PluginManager pluginManager = Bukkit.getServer().getPluginManager();
		pluginManager.registerEvent(Type.ENTITY_DAMAGE, entityListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.PLAYER_INTERACT_ENTITY, playerListener, Priority.Normal, crimson);
		pluginManager.registerEvent(Type.BLOCK_DAMAGE, blockListener, Priority.Normal, crimson);

		//Add to the spout cache
		SpoutManager.getFileManager().addToPreLoginCache(crimson, "http://resources.crimsonrpg.com/s/audio/audiobanhammer.ogg");

		LOGGER.info("[CrimsonAdmin] Plugin enabled.");
	}

}
