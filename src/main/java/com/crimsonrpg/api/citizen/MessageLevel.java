/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.citizen;

import org.bukkit.ChatColor;

/**
 * Represents a level of a message.
 */
public enum MessageLevel {
    /**
     * Represents a normal message.
     */
    NORMAL (ChatColor.WHITE),
    
    /**
     * Represents an info message.
     */
    INFO (ChatColor.YELLOW),
    
    /**
     * Represents a clan message.
     */
    CLAN (ChatColor.GREEN),
    
    /**
     * Represents a plot message.
     */
    PLOT (ChatColor.DARK_AQUA),
    
    /**
     * Represents an error message.
     */
    ERROR (ChatColor.DARK_RED),
    
    /**
     * Represents a private message.
     */
    PRIVATE (ChatColor.LIGHT_PURPLE);
    
    private ChatColor color;

    private MessageLevel(ChatColor color) {
        this.color = color;
    }

    public ChatColor getColor() {
        return color;
    }
}
