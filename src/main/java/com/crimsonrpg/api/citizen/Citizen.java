/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.citizen;

import com.crimsonrpg.flaggables.api.Flaggable;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * Represents a player within the game.
 */
public interface Citizen extends Flaggable {
    
    public String getName();
    
    public Location getLocation();
    
    public boolean hasPermission(String permission);
    
    /**
     * Sends an error message to a citizen.
     *
     * @param string
     */
    @Deprecated
    public void sendError(String string);

    /**
     * Sends an info message to a citizen.
     *
     * @param string
     */
    @Deprecated
    public void sendInfo(String string);

    /**
     * Sends a message to the citizen
     *
     * @param message The message to send.
     */
    public void sendMessage(String message);
    
    /**
     * Sends a message to the citizen with the specified level.
     * 
     * @param message The message to send.
     * @param level The level of the message
     */
    public void sendMessage(String message, MessageLevel level);

    /**
     * Sends a plot-related message to the citizen.
     *
     * @param string
     */
    @Deprecated
    public void sendPlotMessage(String string);

    /**
     * Sends a private message to the citizen.
     *
     * @param string
     */
    @Deprecated
    public void sendPrivateMessage(String string);

    /**
     * Gets the Player of this citizen.
     * 
     * @return The Player
     */
    public Player getBukkitEntity();
    
    /**
     * Gets the SpoutPlayer of this citizen.
     * 
     * @return The SpoutPlayer
     */
    public SpoutPlayer getSpoutEntity();
}
