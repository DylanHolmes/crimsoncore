/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.citizen;

import java.util.List;
import org.bukkit.entity.Player;

/**
 * Represents a manager for {@link Citizen} objects.
 */
public interface CitizenManager {

    /**
     * Gets a citizen from a player object.
     *
     * @param player
     * @return
     */
    public Citizen getCitizen(Player player);

    /**
     * Returns a list of all Citizens.
     * @return 
     */
    public List<Citizen> getCitizenList();

    /**
     * Unloads a citizen from memory.
     * 
     * @param player
     */
    public void unloadCitizen(Player player);
}
