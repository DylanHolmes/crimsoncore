/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.plot;

import com.crimsonrpg.api.plot.Plot;

/**
 * Called when a plot is created.
 */
public class PlotCreateEvent extends PlotEvent {
    private PlotCreateReason reason;
    
    public PlotCreateEvent(Plot plot, PlotCreateReason reason) {
        super(EventType.PLOT_CREATE, plot);
        this.reason = reason;
    }
    
    public enum PlotCreateReason {
        API,
        
        CLAN,
        
        COMMAND;
    }
}
