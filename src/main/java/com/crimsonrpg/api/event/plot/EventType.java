/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.plot;

/**
 *
 * @author simplyianm
 */
public enum EventType {
    PLOT_CHECK_ROLE,
    
    PLOT_CREATE,
    
    PLOT_DISBAND,
    
    PLOT_DEPOSIT,
    
    PLOT_FIRE;
}
