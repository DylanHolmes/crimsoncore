/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.plot;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.plot.Plot;
import com.crimsonrpg.api.plot.Role;

/**
 * Called when a role is checked.
 */
public class PlotCheckRoleEvent extends PlotEvent {
    private final Citizen citizen;
    private Role role;
    
    public PlotCheckRoleEvent(Plot plot, Citizen citizen, Role role) {
        super(EventType.PLOT_CHECK_ROLE, plot);
        this.citizen = citizen;
        this.role = role;
    }

    public Citizen getCitizen() {
        return citizen;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
    
}
