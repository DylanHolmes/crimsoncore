/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.plot;

import com.crimsonrpg.api.plot.Plot;
import org.bukkit.event.Event;

/**
 * Represents a plot-related event.
 */
public abstract class PlotEvent extends Event {
    private final Plot plot;
    private final EventType eventType;

    public PlotEvent(EventType eventType, Plot plot) {
        super(eventType.name());
        this.plot = plot;
        this.eventType = eventType;
    }

    public Plot getPlot() {
        return plot;
    }

    public EventType getEventType() {
        return eventType;
    }
}
