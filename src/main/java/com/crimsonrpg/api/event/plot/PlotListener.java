/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.plot;

import org.bukkit.event.CustomEventListener;
import org.bukkit.event.Event;

/**
 * Listens to plot events.
 */
public class PlotListener extends CustomEventListener {

    public void onPlotCheckRole(PlotCheckRoleEvent event) {
    }

    @Override
    public void onCustomEvent(Event event) {
        if (!(event instanceof PlotEvent)) {
            return;
        }

        switch (((PlotEvent) event).getEventType()) {
            case PLOT_CHECK_ROLE:
                this.onPlotCheckRole((PlotCheckRoleEvent) event);
                return;
                
                
        }
    }
}
