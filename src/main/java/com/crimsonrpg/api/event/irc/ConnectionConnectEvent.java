/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.irc;

import com.crimsonrpg.api.irc.Connection;

/**
 * Called when a connection successfully connects.
 */
public class ConnectionConnectEvent extends IRCEvent {

    public ConnectionConnectEvent(Connection connection) {
        super(EventType.CONNECTION_CONNECT, connection);
    }
    
}
