/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.irc;

import com.crimsonrpg.api.irc.Connection;

/**
 * Called when a connection disconnects.
 */
public class ConnectionDisconnectEvent extends IRCEvent {

    public ConnectionDisconnectEvent(Connection connection) {
        super(EventType.CONNECTION_DISCONNECT, connection);
    }
    
}