/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

import com.crimsonrpg.api.clan.Clan;
import org.bukkit.event.Event;

/**
 * Represents a Clan-related event.
 */
public abstract class ClanEvent extends Event {
    private final EventType eventType;
    private final Clan clan;

    public ClanEvent(EventType eventType, Clan clan) {
        super(eventType.name());
        this.eventType = eventType;
        this.clan = clan;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Clan getClan() {
        return clan;
    }
}
