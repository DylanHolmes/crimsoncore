/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

/**
 *
 * @author simplyianm
 */
public enum EventType {
    CLAN_CREATE,
    
    CLAN_DISBAND,
    
    CLAN_RENAME;
}
