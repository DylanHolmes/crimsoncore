/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

import com.crimsonrpg.api.clan.Clan;

/**
 * Called when a clan is disbanded.
 */
public class ClanCreateEvent extends ClanEvent {
    private final ClanCreateReason reason;

    public ClanCreateEvent(Clan clan, ClanCreateReason reason) {
        super(EventType.CLAN_CREATE, clan);
        this.reason = reason;
    }
    
    public enum ClanCreateReason {
        COMMAND,
        
        UNKNOWN;
    }
}
