/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

import com.crimsonrpg.api.clan.Clan;

/**
 * Called when a clan is disbanded.
 */
public class ClanDisbandEvent extends ClanEvent {
    private final ClanDisbandReason reason;

    public ClanDisbandEvent(Clan clan, ClanDisbandReason reason) {
        super(EventType.CLAN_DISBAND, clan);
        this.reason = reason;
    }
    
    public enum ClanDisbandReason {
        COMMAND,
        
        UNKNOWN;
    }
}
