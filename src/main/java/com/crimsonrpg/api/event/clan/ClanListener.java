/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

import org.bukkit.event.CustomEventListener;
import org.bukkit.event.Event;

/**
 *
 * @author simplyianm
 */
public class ClanListener extends CustomEventListener {
    public void onClanCreate(ClanCreateEvent event) {}
    
    public void onClanDisband(ClanDisbandEvent event) {}
    
    public void onClanRename(ClanRenameEvent event) {}
    
    @Override
    public void onCustomEvent(Event event) {
        if (!(event instanceof ClanEvent)) return;
        
        switch (((ClanEvent) event).getEventType()) {
            case CLAN_CREATE:
                this.onClanCreate((ClanCreateEvent) event);
                break;
                
            case CLAN_DISBAND:
                this.onClanDisband((ClanDisbandEvent) event);
                break;
            
            case CLAN_RENAME:
                this.onClanRename((ClanRenameEvent) event);
                break;
        }
    }
}
