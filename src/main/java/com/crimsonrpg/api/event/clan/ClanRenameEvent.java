/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.event.clan;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.util.MD5Helper;
import org.bukkit.event.Cancellable;

/**
 * Called when a clan is renamed.
 */
public class ClanRenameEvent extends ClanEvent implements Cancellable {
    private final Citizen renamer;
    private String hash;
    private String newName;
    private boolean cancelled;
    
    public ClanRenameEvent(Clan clan, Citizen renamer, String hash, String newName) {
        super(EventType.CLAN_RENAME, clan);
        this.renamer = renamer;
        this.hash = hash;
        this.newName = newName;
    }

    public Citizen getRenamer() {
        return renamer;
    }

    public String getNewName() {
        return newName;
    }

    public String getHash() {
        return hash;
    }

    public void setNewName(String newName) {
        this.hash = MD5Helper.md5(newName);
        this.newName = newName;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
