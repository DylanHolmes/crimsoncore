/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author simplyianm
 */
public class WikiStrip {
	/**
	 * Checks for wiki markup. (Automagical documentation!)
	 */
	private static Pattern wikiLinks = Pattern.compile("\\[\\[([\\s\\w]+)\\]\\]");
	
	public static String stripWiki(String raw) {
		String wikiStripped = raw;

		//Strip wiki tags
		Matcher wikiStrip = wikiLinks.matcher(wikiStripped);
		while (wikiStrip.find()) {
			wikiStripped = wikiStripped.replaceAll(wikiStrip.group(), wikiStrip.group(1));
		}
		return wikiStripped;
	}
}
