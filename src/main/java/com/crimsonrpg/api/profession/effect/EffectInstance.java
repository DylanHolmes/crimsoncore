/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

import com.crimsonrpg.api.citizen.Citizen;
import org.bukkit.entity.LivingEntity;

/**
 *
 * @author simplyianm
 */
public class EffectInstance {
	private final Citizen caster;
	private LivingEntity target;

	public EffectInstance(Citizen caster, LivingEntity target) {
		this.caster = caster;
		this.target = target;
	}

	public Citizen getCaster() {
		return caster;
	}

	public LivingEntity getTarget() {
		return target;
	}

	
}
