/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzEffects;
import com.crimsonrpg.api.profession.skill.Skill;

/**
 * Represents an effect that does something at a set interval.
 */
public abstract class RepeatingEffect extends Effect {
	/**
	 * Represents an effect that does something at a set interval.
	 * @param skill
	 * @param delay 
	 */
	public RepeatingEffect(Skill skill) {
		super(skill);
	}

	@Override
	public void update(Citizen citizen) {
		int since = citizen.getFlag(FlagCzEffects.class).getSecondsRemaining(this);
		if (since % getInterval(citizen) == 0) {
			onInterval(citizen);
		}
	}

	public abstract void onInterval(Citizen citizen);

	public abstract int getInterval(Citizen citizen);

}
