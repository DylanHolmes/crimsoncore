/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.skill.Skill;

/**
 *
 * @author simplyianm
 */
public abstract class EternalEffect extends Effect {
	public EternalEffect(Skill skill) {
		super(skill);
	}

	@Override
	public final EffectResult onApply(Citizen citizen) {
		return EffectResult.ETERNAL;
	}

	@Override
	public void update(Citizen citizen) {
	}

	@Override
	public final void onRemove(Citizen citizen) {
		return;
	}

	@Override
	public int getSeconds(Citizen citizen) {
		return 0;
	}

}
