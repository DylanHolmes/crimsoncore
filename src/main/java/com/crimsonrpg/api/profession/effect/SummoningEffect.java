/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.skill.Skill;

/**
 *
 * @author simplyianm
 */
public abstract class SummoningEffect extends Effect {
	public SummoningEffect(Skill skill) {
		super(skill);
	}

	@Override
	public EffectResult onApply(Citizen citizen) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void update(Citizen citizen) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void onRemove(Citizen citizen) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
