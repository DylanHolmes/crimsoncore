/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

/**
 *
 * @author simplyianm
 */
public enum EffectResult {
	SUCCESS(true),
	ETERNAL(true),
	INCOMPATIBLE(false);

	private boolean success;

	private EffectResult(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

}
