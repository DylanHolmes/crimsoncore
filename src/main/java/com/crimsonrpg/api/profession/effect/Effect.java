/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.effect;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzEffects;
import com.crimsonrpg.api.profession.description.Describable;
import com.crimsonrpg.api.profession.description.Description;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.util.MD5Helper;
import java.util.List;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents an effect.
 */
public abstract class Effect implements Describable<Effect> {
	private final Skill skill;

	private boolean buff = true;

	private List<Citizen> citizens;

	//Description
	private String id = null;

	private String name = null;

	private Description<Effect> description;

	private Citizen caster = null;

	/**
	 * Creates a new effect.
	 * 
	 * @param skill
	 */
	protected Effect(Skill skill) {
		this.skill = skill;
	}

	/**
	 * Sets the ID of this effect.
	 * @param id 
	 */
	protected void id(String id) {
		if (this.id == null) {
			this.id = MD5Helper.md5(getPlugin().getDescription().getFullName() + '_' + id);
		}
	}

	/**
	 * Sets the name of this effect.
	 * @param name 
	 */
	protected void name(String name) {
		if (this.name == null) {
			this.name = name;
		}
	}

	/**
	 * Sets the description of this effect.
	 * @param description 
	 */
	protected void description(String description) {
		if (this.description == null) {
			this.description = CrimsonAPI.getProfessionManager().getEffectDescriptionLoader().loadDescription(getSkill().getAttribId(), description);
		}
	}

	/**
	 * Makes the effect a debuff.
	 */
	protected void debuff() {
		this.buff = false;
	}

	/**
	 * Gets the ID of this effect.
	 * 
	 * @return 
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the name of this effect.
	 * 
	 * @return 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the description of this effect.
	 * 
	 * @return 
	 */
	public Description<Effect> getDescription() {
		return description;
	}

	/**
	 * Gets the seconds this effect lasts.
	 * 
	 * @param citizen The citizen that will receive the effect
	 * @return 
	 */
	public abstract int getSeconds(Citizen citizen);

	/**
	 * Returns true if this effect is a buff.
	 * 
	 * @return 
	 */
	public boolean isBuff() {
		return buff;
	}

	/**
	 * Returns true if this effect is a debuff.
	 * 
	 * @return 
	 */
	public boolean isDebuff() {
		return !buff;
	}

	/**
	 * Returns true if this effect is eternal.
	 * 
	 * @return 
	 */
	public boolean isEternal() {
		return getSeconds(null) <= 0;
	}

	/**
	 * Gets the caster of the effect.
	 * 
	 * @return 
	 */
	public Citizen getCaster() {
		return caster;
	}

	/**
	 * Gets the skill this effect is associated with.
	 * 
	 * @return 
	 */
	public final Skill getSkill() {
		return skill;
	}

	public List<Citizen> getCitizens() {
		return citizens;
	}

	/**
	 * Applies the effect without doing the actual application effect.
	 * 
	 * @param citizen 
	 */
	public final void applyNoEffect(Citizen citizen) {
		citizens.add(citizen);
	}

	/**
	 * Applies the effect.
	 * 
	 * @param citizen 
	 */
	public EffectResult apply(Citizen citizen) {
		applyNoEffect(citizen);
		return onApply(citizen);
	}

	/**
	 * This method is called every second (20 ticks).
	 */
	public final void update() {
		for (Citizen citizen : citizens) {
			update(citizen);
			FlagCzEffects flag = citizen.getFlag(FlagCzEffects.class);
			long whenApplied = flag.getSecondsRemaining(this);
			if (whenApplied < 0) {
				remove(citizen);
			}
		}
	}

	/**
	 * Removes the citizen from the effect.
	 * 
	 * @param citizen 
	 */
	public void remove(Citizen citizen) {
		citizens.remove(citizen);
		citizen.getFlag(FlagCzEffects.class).removeEffectInternal(this);
		onRemove(citizen);
	}

	/**
	 * Sets the caster of the effect, if any.
	 * @param caster 
	 */
	public void setCaster(Citizen caster) {
		if (this.caster == null) {
			this.caster = caster;
		}
	}

	/**
	 * Returns true if this effect affects the given citizen.
	 * 
	 * @param citizen
	 * @return 
	 */
	public boolean isAffecting(Citizen citizen) {
		return citizens.contains(citizen);
	}

	/**
	 * Gets the plugin this effect is associated with.
	 * 
	 * @return 
	 */
	public final JavaPlugin getPlugin() {
		return skill.getPlugin();
	}

	/**
	 * Called when the effect is applied.
	 * 
	 * @param citizen 
	 */
	public abstract EffectResult onApply(Citizen citizen);

	/**
	 * Called when the effect is updated. (Once per second)
	 * 
	 * @param citizen 
	 */
	public abstract void update(Citizen citizen);

	/**
	 * Called when the effect is removed.
	 * 
	 * @param citizen 
	 */
	public abstract void onRemove(Citizen citizen);

}
