/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.description;

/**
 *
 * @author simplyianm
 */
public abstract class VariableGraph {
	public abstract double getValue(double percent, double low, double high);

	public int getValueInt(double percent, double low, double high) {
		return (int) Math.round(getValue(percent, low, high));
	}

}