/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.description;

import java.util.Map;

/**
 *
 * @author simplyianm
 */
public interface Description<T extends Describable> {
	public String getRawDescription();

	public String getDescription(int level);

	public String getDescription(int level, String color);

	public double getVar(String name, int level);

	public int getVarInt(String name, int level);

	public Map<String, DescriptionVariable> getVars();

}
