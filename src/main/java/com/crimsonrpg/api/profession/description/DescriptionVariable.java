/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.description;

/**
 *
 * @author simplyianm
 */
public class DescriptionVariable {
	private double low;

	private double high;

	private int maxLevel;

	private String name;

	private VariableGraph graph;

	public DescriptionVariable(double low, double high, int maxLevel, String name, VariableGraph graph) {
		this.low = low;
		this.high = high;
		this.maxLevel = maxLevel;
		this.name = name;
		this.graph = graph;
	}

	public String getName() {
		return name;
	}

	public double getValue(int level) {
		return graph.getValue((double) (level - 1) / (double) (maxLevel - 1), low, high);
	}

	public int getValueInt(int level) {
		return graph.getValueInt((double) (level - 1) / (double) (maxLevel - 1), low, high);
	}

}
