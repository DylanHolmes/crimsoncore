/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.description;

/**
 *
 * @author simplyianm
 */
public interface DescriptionLoader<T extends Describable> {
	public Description<T> loadDescription(int maxLevel, String raw);
}
