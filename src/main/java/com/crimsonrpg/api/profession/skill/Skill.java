/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzProfession;
import com.crimsonrpg.core.profession.CrimsonProfessions;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.description.Describable;
import com.crimsonrpg.api.profession.description.Description;
import java.util.logging.Level;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents a skill.
 */
public abstract class Skill implements Describable {
	private byte id = 0x00;

	private String name = null;

	private Attribute attribute = null;

	private Description<Skill> description = null;

	protected Skill(Attribute attribute) {
		this.attribute = attribute;
	}

	protected void id(int id) {
		if (!(this.id <= 0x0 || this.id > 0xff)) {
			return;
		}

		int realId = id & 0xff;
		if (id != realId) {
			CrimsonProfessions.LOGGER.log(Level.WARNING, "Invalid id '0{0}' found; truncating to '0" + Integer.toHexString(realId) + "'.", Integer.toHexString(id));
		}

		id = (byte) realId;
	}

	protected void name(String name) {
		if (this.name == null) {
			this.name = name;
		}
	}

	protected void description(String description) {
		if (this.description == null) {
			this.description = CrimsonAPI.getProfessionManager().getSkillDescriptionLoader().loadDescription(attribute.getMaxLevel(), description);
		}
	}

	public int getId() {
		return (attribute.getId() << 8) | id;
	}

	public short getAttribId() {
		return (short) ((attribute.getInternalId() << 8) | id);
	}

	public byte getInternalId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Description<Skill> getDescription() {
		return description;
	}

	/**
	 * Gets a variable.
	 * @param name
	 * @param level
	 * @return 
	 */
	public final double getVar(String name, int level) {
		return description.getVar(name, level);
	}

	/**
	 * Gets an integer variable.
	 * @param name
	 * @param level
	 * @return 
	 */
	public final int getVarInt(String name, int level) {
		return description.getVarInt(name, level);
	}

	/**
	 * Gets a variable.
	 * @param name
	 * @param level
	 * @return 
	 */
	public final double $(String name, int level) {
		return getVar(name, level);
	}

	public final double $(String name, Citizen citizen) {
		if (citizen == null) {
			return -1;
		}
		return $(name, citizen.getFlag(FlagCzProfession.class).getSkillLevel(this));
	}

	/**
	 * Gets an integer variable.
	 * @param name
	 * @param level
	 * @return 
	 */
	public final int $$(String name, int level) {
		return getVarInt(name, level);
	}

	public final int $$(String name, Citizen citizen) {
		if (citizen == null) {
			return -1;
		}
		return $$(name, citizen.getFlag(FlagCzProfession.class).getSkillLevel(this));
	}

	/**
	 * Triggers the skill for the player.
	 * @param user 
	 */
	public final void trigger(Citizen user) {
		if (shouldUse(user)) {
			use(user);
		}
	}

	/**
	 * Gets the plugin this skill is associated with.
	 * @return 
	 */
	public JavaPlugin getPlugin() {
		return attribute.getPlugin();
	}

	/**
	 * Activates the skill for the citizen.
	 * 
	 * @param user 
	 */
	public abstract void activate(Citizen user);

	/**
	 * Checks if the citizen can use the skill.
	 * @param user
	 * @return 
	 */
	protected abstract boolean shouldUse(Citizen user);

	/**
	 * Uses the skill.
	 * @param user
	 * @return 
	 */
	protected abstract void use(Citizen user);

}
