/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill.active;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.api.profession.effect.EffectResult;
import com.crimsonrpg.api.profession.skill.SkillResult;

/**
 *
 * @author simplyianm
 */
public class EffectSelfSkill extends ActiveSkill {
	private Effect effect = null;

	protected EffectSelfSkill(Attribute attribute) {
		super(attribute);
	}

	@Override
	public SkillResult useSkill(Citizen user) {
		if (effect == null) {
			throw new IllegalStateException("An effect has not been specified!");
		}
		EffectResult result = effect.apply(user);
		if (result.isSuccess()) {
			return SkillResult.NORMAL;
		}
		return SkillResult.FAIL;
	}

	protected void effect(Effect effect) {
		if (this.effect == null) {
			this.effect = effect;
		}
	}

}
