/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill;

/**
 *
 * @author simplyianm
 */
public enum SkillResult {
	CANCELLED,
	IGNORE,
	INVALID_TARGET,
	FAIL,
	LOW_MANA,
	LOW_HEALTH,
	LOW_STAMINA,
	MISSING_REAGENT,
	NORMAL,
	PASSIVE,
	REMOVED_EFFECT,
	SKIP_POST_USAGE,
	START_DELAY;

}