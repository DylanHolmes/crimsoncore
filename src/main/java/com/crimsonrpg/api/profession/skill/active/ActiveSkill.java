/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill.active;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzCasting;
import com.crimsonrpg.api.flag.FlagCzEnergy;
import com.crimsonrpg.api.flag.FlagCzProfession;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.player.PlayerListener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author simplyianm
 */
public abstract class ActiveSkill extends Skill {
	private Map<Player, Integer> castingTasks = new HashMap<Player, Integer>();

	private int energy = 0;

	/**
	 * Casting time in milliseconds.
	 */
	private long castTime = -1;

	protected ActiveSkill(Attribute attribute) {
		super(attribute);
		Bukkit.getServer().getPluginManager().registerEvent(Event.Type.PLAYER_MOVE, new PlayerListener() {
			@Override
			public void onPlayerMove(PlayerMoveEvent event) {
				Integer castingTask = castingTasks.get(event.getPlayer());
				if (castingTask != null) {
					Bukkit.getScheduler().cancelTask(castingTask);
				}
				//You have interrupted your spell casting.
			}

		}, Priority.Normal, getPlugin());
	}

	public void activate(Citizen user) {
	}

	protected void energy(int energy) {
		if (energy == 0) {
			this.energy = energy;
		}
	}

	/**
	 * Sets the cast time of the spell.
	 * 
	 * @param time The time to cast the spell.
	 */
	protected void castTime(double time) {
		if (this.castTime < 0) {
			this.castTime = (long) Math.floor(time * 1000);
		}
	}

	@Override
	public boolean shouldUse(Citizen user) {
		if (user.getFlag(FlagCzCasting.class).isCasting()) {
			//You're already casting a spell.
			return false;
		}

		if (user.getFlag(FlagCzProfession.class).getSkillLevel(this) < 1) {
			//You don't have this skill.
			return false;
		}

		if (user.getFlag(FlagCzEnergy.class).getEnergy() < energy) {
			//You don't have enough energy.
			return false;
		}

		return true;
	}

	@Override
	public final void use(final Citizen user) {
		castingTasks.put(user.getBukkitEntity(), Bukkit.getScheduler().scheduleSyncDelayedTask(getPlugin(), new Runnable() {
			public void run() {
				SkillResult result = useSkill(user);
				processResult(user, result);
			}

		}, castTime));
	}

	public void processResult(Citizen user, SkillResult result) {
		//TODO send messages
		user.getFlag(FlagCzEnergy.class).subtract(energy);
	}

	public abstract SkillResult useSkill(Citizen user);

	public long getCastTime() {
		return castTime;
	}

	public boolean isTimed() {
		return castTime > 0;
	}

}
