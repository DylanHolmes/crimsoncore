/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill.passive;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.flag.FlagCzProfession;
import com.crimsonrpg.api.profession.effect.Effect;

/**
 *
 * @author simplyianm
 */
public abstract class PassiveSkill extends Skill {
	private Effect effect;

	public PassiveSkill(Attribute attribute) {
		super(attribute);
	}

	/**
	 * Sets the effect this skill will have.
	 * 
	 * @param effect 
	 */
	protected void effect(Effect effect) {
		this.effect = effect;
	}

	//These methods will never be called.
	@Override
	public boolean shouldUse(Citizen user) {
		return user.getFlag(FlagCzProfession.class).getSkillLevel(this) > 0;
	}

}
