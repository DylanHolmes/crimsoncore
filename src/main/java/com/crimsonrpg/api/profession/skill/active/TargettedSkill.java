/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill.active;

import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.api.citizen.Citizen;
import java.util.HashSet;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
public abstract class TargettedSkill extends ActiveSkill {
	public static final HashSet<Byte> transparent = new HashSet<Byte>(22) {
		{
			add((byte) Material.AIR.getId());
			add((byte) Material.SNOW.getId());
			add((byte) Material.REDSTONE_WIRE.getId());
			add((byte) Material.TORCH.getId());
			add((byte) Material.REDSTONE_TORCH_OFF.getId());
			add((byte) Material.REDSTONE_TORCH_ON.getId());
			add((byte) Material.RED_ROSE.getId());
			add((byte) Material.YELLOW_FLOWER.getId());
			add((byte) Material.SAPLING.getId());
			add((byte) Material.LADDER.getId());
			add((byte) Material.STONE_PLATE.getId());
			add((byte) Material.WOOD_PLATE.getId());
			add((byte) Material.CROPS.getId());
			add((byte) Material.LEVER.getId());
			add((byte) Material.WATER.getId());
			add((byte) Material.STATIONARY_WATER.getId());
			add((byte) Material.RAILS.getId());
			add((byte) Material.POWERED_RAIL.getId());
			add((byte) Material.DETECTOR_RAIL.getId());
			add((byte) Material.DIODE_BLOCK_OFF.getId());
			add((byte) Material.DIODE_BLOCK_ON.getId());
		}

	};

	private int range = 50;

	protected TargettedSkill(Attribute attribute) {
		super(attribute);
	}

	protected void range(int range) {
		if (this.range == 50) {
			this.range = range;
		}
	}

	/**
	 * Returns the first LivingEntity in the line of sight of a Player.
	 * 
	 * @param player
	 *            the player being checked
	 * @param maxDistance
	 *            the maximum distance to search for a target
	 * @return the player's target or null if no target is found
	 */
	public static LivingEntity getPlayerTarget(Player player, int maxDistance) {
		if (player.getLocation().getBlockY() > player.getLocation().getWorld().getMaxHeight()) {
			return null;
		}
		List<Block> lineOfSight = player.getLineOfSight(transparent, maxDistance);
		List<Entity> nearbyEntities = player.getNearbyEntities(maxDistance, maxDistance, maxDistance);
		for (Entity entity : nearbyEntities) {
			if (entity instanceof LivingEntity) {
				Location eLoc = entity.getLocation();
				for (Block block : lineOfSight) {
					Location bLoc = block.getLocation();
					if (eLoc.getBlockX() == bLoc.getBlockX() && eLoc.getBlockZ() == bLoc.getBlockZ() && Math.abs(eLoc.getBlockY() - bLoc.getBlockY()) < 2) {
						return (LivingEntity) entity;
					}
				}
			}
		}
		return null;
	}

	@Override
	public SkillResult useSkill(Citizen user) {
		LivingEntity target = getPlayerTarget(user.getBukkitEntity(), range);
		return use(user, target);
	}

	/**
	 * Called when the skill is used.
	 * 
	 * @param user
	 * @param target
	 * @return 
	 */
	public abstract SkillResult use(Citizen user, LivingEntity target);

}
