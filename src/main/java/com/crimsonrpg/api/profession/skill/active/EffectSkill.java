/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.skill.active;

import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.api.profession.attribute.Attribute;
import com.crimsonrpg.api.profession.skill.SkillResult;
import com.crimsonrpg.api.citizen.Citizen;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
public abstract class EffectSkill extends TargettedSkill {
	private Effect effect = null;

	public EffectSkill(Attribute attribute) {
		super(attribute);
	}

	@Override
	public SkillResult use(Citizen user, LivingEntity target) {
		if (effect == null) {
			throw new IllegalStateException("An effect has not been specified!");
		}

		if (!(target instanceof Player)) {
			return SkillResult.INVALID_TARGET;
		}

		Player ptarget = (Player) target;

		//TODO use Citizen
		effect.apply(null);

		return SkillResult.NORMAL;
	}

	protected void effect(Effect effect) {
		if (effect == null) {
			this.effect = effect;
		}
	}

}
