/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.profession;

import com.crimsonrpg.core.profession.CrimsonProfessions;
import com.crimsonrpg.api.profession.attribute.Attribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents a profession.
 */
public abstract class Profession {
	private final JavaPlugin plugin;

	private byte id = 0x00;

	private String name = null;

	private String description = null;

	private Map<String, Attribute> attributes;

	protected Profession(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	/**
	 * Sets the id of the profession.
	 * Must be one byte.
	 * 
	 * @param id The id to set.
	 */
	protected void id(int id) {
		if (!(this.id <= 0x0 || this.id > 0xff)) {
			return;
		}

		int realId = id & 0xff;
		if (id != realId) {
			CrimsonProfessions.LOGGER.log(Level.WARNING, "Invalid id '0{0}' found; truncating to '0" + Integer.toHexString(realId) + "'.", Integer.toHexString(id));
		}

		id = (byte) realId;
	}

	/**
	 * Sets the name of the profession.
	 * 
	 * @param name The name to set.
	 */
	protected void name(String name) {
		if (this.name == null) {
			this.name = name;
		}
	}

	/**
	 * Sets the description of the profession.
	 * 
	 * @param description The description to set.
	 */
	protected void description(String description) {
		if (this.description == null) {
			this.description = description;
		}
	}

	/**
	 * Sets the attributes of the profession.
	 * 
	 * @param attributes The attributes to set.
	 */
	protected void attributes(Attribute... attributes) {
		if (this.attributes == null) {
			this.attributes = new HashMap<String, Attribute>();
			for (Attribute attribute : attributes) {
				this.attributes.put(attribute.getName(), attribute);
			}
		}
	}

	/**
	 * Gets the id of the profession.
	 * 
	 * @return The id of the profession.
	 */
	public byte getId() {
		return id;
	}

	/**
	 * Gets the name of the profession.
	 * 
	 * @return The name of the profession.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the attributes of the profession.
	 * 
	 * @return The attributes of the profession.
	 */
	public List<Attribute> getAttributes() {
		return new ArrayList<Attribute>(attributes.values());
	}

	public JavaPlugin getPlugin() {
		return plugin;
	}

}
