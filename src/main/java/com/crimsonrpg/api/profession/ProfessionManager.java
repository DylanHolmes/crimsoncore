/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession;

import com.crimsonrpg.api.profession.description.DescriptionLoader;
import com.crimsonrpg.api.profession.description.VariableGraph;
import com.crimsonrpg.api.profession.profession.Profession;
import com.crimsonrpg.api.profession.skill.Skill;
import com.crimsonrpg.api.profession.effect.Effect;

/**
 *
 * @author simplyianm
 */
public interface ProfessionManager {
	public void registerGraph(String name, VariableGraph graph);

	public VariableGraph getGraph(String name);

	public Profession getProfession(String name);

	public void registerProfession(Profession profession);

	public void registerProfessions(Profession... professions);

	public void registerEffect(Effect effect);

	public Effect getEffect(String id);

	public DescriptionLoader<Skill> getSkillDescriptionLoader();

	public DescriptionLoader<Effect> getEffectDescriptionLoader();

}
