/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.profession.attribute;

import com.crimsonrpg.core.profession.CrimsonProfessions;
import com.crimsonrpg.api.profession.profession.Profession;
import com.crimsonrpg.api.profession.skill.Skill;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Represents an attribute of a skill.
 */
public abstract class Attribute {
	private byte id = 0x00;

	private String name = null;

	private String description = null;

	private Map<String, Skill> skills = null;

	private int maxLevel = -1;

	private final Profession profession;

	public Attribute(Profession profession) {
		this.profession = profession;
	}

	/**
	 * Sets the id of the attribute.
	 * 
	 * @param id The id to set.
	 */
	protected void id(int id) {
		if (!(this.id <= 0x0 || this.id > 0xff)) {
			return;
		}

		int realId = id & 0xff;
		if (id != realId) {
			CrimsonProfessions.LOGGER.log(Level.WARNING, "Invalid id '0{0}' found; truncating to '0" + Integer.toHexString(realId) + "'.", Integer.toHexString(id));
		}

		id = (byte) realId;
	}

	/**
	 * Sets the name of the attribute.
	 * 
	 * @param name The name to set.
	 */
	protected void name(String name) {
		if (this.name == null) {
			this.name = name;
		}
	}

	/**
	 * Sets the description of the attribute.
	 * 
	 * @param description The description to set.
	 */
	protected void description(String description) {
		if (this.description == null) {
			this.description = description;
		}
	}

	/**
	 * Sets the skills of the attribute.
	 * 
	 * @param skills The skills to set.
	 */
	protected void skills(Skill... skills) {
		if (this.skills == null) {
			this.skills = new HashMap<String, Skill>();
			for (Skill skill : skills) {
				this.skills.put(skill.getName(), skill);
			}
		}
	}

	/**
	 * Sets the maximum level this attribute can have.
	 * 
	 * @param maxLevel The level to set.
	 */
	protected void maxLevel(int maxLevel) {
		if (this.maxLevel < 1) {
			this.maxLevel = maxLevel;
		}
	}

	/**
	 * Gets the max level of the attribute.
	 * 
	 * @return The max level of the attribute.
	 */
	public int getMaxLevel() {
		return maxLevel;
	}

	/**
	 * Gets the internal id of the attribute.
	 * 
	 * @return The internal id of the attribute.
	 */
	public byte getInternalId() {
		return id;
	}

	/**
	 * Gets the name of this attribute.
	 * 
	 * @return The name of this attribute.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the profession of this attribute.
	 * 
	 * @return The profession of this attribute.
	 */
	public Profession getProfession() {
		return profession;
	}

	/**
	 * Gets the full, unique id of this attribute.
	 * 
	 * @return The id of this attribute.
	 */
	public short getId() {
		return (short) ((profession.getId() << 8) | id);
	}

	public JavaPlugin getPlugin() {
		return profession.getPlugin();
	}
}
