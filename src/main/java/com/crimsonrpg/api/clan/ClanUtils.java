/*
 * This code is released under the
 * Creative Commons by-nc license.
 * Don't claim this as your own and don't
 * use this code for profit without the permission
 * of the code author.
 */
package com.crimsonrpg.api.clan;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.citizen.MessageLevel;
import com.crimsonrpg.api.flag.FlagCnRanks;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for managing clans.
 */
public class ClanUtils {
    public static void sendClanMessage(Clan clan, String message) {
        for (Citizen citizen : clan.getFlag(FlagCnRanks.class).getOnlineMembers()) {
            citizen.sendMessage(message, MessageLevel.CLAN);
        }
    }
    
    public static List<Citizen> getOnlineMembers(Clan clan) {
        List<Citizen> citizens = new ArrayList<Citizen>();
        return citizens;
    }
}
