/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.clan;

import com.crimsonrpg.flaggables.api.Flaggable;

/**
 * Represents a clan.
 */
public interface Clan extends Flaggable {
    /**
     * Gets the unique id of this clan.
     * 
     * @return 
     */
    public String getId();
}
