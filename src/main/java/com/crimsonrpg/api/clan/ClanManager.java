/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.clan;

import com.crimsonrpg.api.event.clan.ClanCreateEvent;
import com.crimsonrpg.api.event.clan.ClanDisbandEvent;
import com.crimsonrpg.api.event.clan.ClanRenameEvent;
import java.util.List;

import com.crimsonrpg.flaggables.api.Flag;

/**
 * Represents a manager of Clans.
 */
public interface ClanManager {
    /**
     * Gets a clan from its id.
     * 
     * @param id
     * @return 
     */
    public Clan getClan(String id);
    
    /**
     * Creates a clan.
     * 
     * @param id
     * @param flags
     * @return 
     */
    public Clan createClan(String id);
    
    /**
     * Creates a clan with the given reason.
     * 
     * @param id
     * @param reason
     * @return 
     */
    public Clan createClan(String id, ClanCreateEvent.ClanCreateReason reason);
    
    /**
     * Disbands a clan.
     * 
     * @param clan 
     */
    public void disbandClan(Clan clan);
    
    /**
     * Disbands a clan.
     * 
     * @param clan The clan to disband.
     * @param reason The reason the clan was disbanded.
     */
    public void disbandClan(Clan clan, ClanDisbandEvent.ClanDisbandReason reason);
    
    /**
     * Changes the id of a clan.
     * 
     * @param clan The clan to change the id of.
     * @param id The new id.
     */
    public void changeId(Clan clan, String id);
    
    /**
     * Gets all loaded clans on the server.
     * 
     * @return The clans
     */
    public List<Clan> getClans();
}