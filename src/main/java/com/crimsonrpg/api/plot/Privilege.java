/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.plot;

/**
 * Represents a privilege that a citizen can hold regarding plots.
 */
public enum Privilege {
    /**
     * Server admin privileges
     */
    
    /**
     * Adminifies a plot.
     */
    SADMIN_ADMINIFY (Category.SADMIN),
    
    /**
     * Sizes a plot to the given size.
     */
    SADMIN_SIZE (Category.SADMIN),
    
    /**
     * Owner privileges
     */
    
    /**
     * Adds a coowner to the plot.
     */
    OWNER_COOWNER (Category.OWNER),
    
    /**
     * Allows a player to disband the plot.
     */
    OWNER_DISBAND (Category.OWNER),
    
    /**
     * Allows a player to downgrade a plot.
     */
    OWNER_DOWNGRADE (Category.OWNER),
    
    /**
     * Allows a player to rename the plot.
     */
    OWNER_RENAME (Category.OWNER),

    /**
     * Allows a player to shrink the plot.
     */
    OWNER_SELL (Category.OWNER),

    /**
     * Allows a player to shrink the plot.
     */
    OWNER_SHRINK (Category.OWNER),
    
    /**
     * Allows a player to upgrade a plot.
     */
    OWNER_UPGRADE (Category.OWNER),
    
    /**
     * Admin privileges
     */
    
    /**
     * Allows a player to withdraw money from the plot.
     */
    ADMIN_WITHDRAW (Category.ADMIN),

    /**
     * Allows a player to expand the plot.
     */
    ADMIN_EXPAND (Category.ADMIN),

    /**
     * Moderator privileges
     */
    
    /**
     * Allows a player to add members to the plot.
     */
    MOD_ADD_MEMBERS (Category.MOD),

    /**
     * Allows a player to demote plot members.
     */
    MOD_DEMOTE (Category.MOD),
    
    /**
     * Allows a player to fire an NPC.
     */
    MOD_FIRE_NPC (Category.MOD),
    
    /**
     * Allows a player to kick out other members.
     */
    MOD_KICK (Category.MOD),
    
     /**
     * Allows a player to create a banker NPC.
     */
    MOD_NPC_BANKER (Category.MOD),
    
    /*
     * Allows a player to create a vendor NPC.
     */
    
    MOD_NPC_VENDOR (Category.MOD),
    
    /*
     * Allows a player to create a guard NPC.
     */
    MOD_NPC_GUARD (Category.MOD),
    
    /**
     * Allows a player to promote plot members.
     */
    MOD_PROMOTE (Category.MOD),

    /**
     * Allows a player to add zones to the plot.
     */
    MOD_ZONE (Category.MOD),
    
    /**
     * Builder privileges
     */
    
    /**
     * Allows a player to build in the plot.
     */
    BUILDER_PLACE (Category.BUILDER),

    /**
     * Allows a player to destroy in the plot.
     */
    BUILDER_DESTROY (Category.BUILDER),
    
    /**
     * Allows a player to burn stuff in the plot.
     */
    BUILDER_FIRE (Category.BUILDER),
    
    /**
     * Member privileges
     */
    
    /**
     * Allows a player to donate to the plot.
     */
    MEMBER_DEPOSIT (Category.MEMBER),

    /**
     * Allows a player to use doors, levers, etc in the plot.
     */
    MEMBER_USE (Category.MEMBER),
    
    /**
     * Allows a player to inspect other members of the plot.
     */
    MEMBER_INSPECT (Category.MEMBER),
    
    /**
     * Guest privileges
     */
    
    /**
     * Allows a player to enter the plot.
     */
    GUEST_ENTER (Category.GUEST);

    private Category category;
    
    private Privilege(Category category) {
        this.category = category;
    }

    /**
     * Gets the category of the privilege.
     * 
     * @return 
     */
    public Category getCategory() {
        return category;
    }
    
    /**
     * Represents a category of plot privileges.
     */
    public enum Category {
        /**
         * Server admin
         */
        SADMIN,
        
        /**
         * Owner
         */
        OWNER,
        
        /**
         * Admin
         */
        ADMIN,
        
        /**
         * Mod
         */
        MOD,

        /**
         * Builder
         */
        BUILDER,

        /**
         * Member
         */
        MEMBER,
        
        /**
         * Guest
         */
        GUEST;
    }
}
