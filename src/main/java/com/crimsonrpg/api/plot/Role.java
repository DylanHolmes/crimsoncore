/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.plot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a role a player can partake in a plot.
 */
public class Role {
    /**
     * Represents a server admin. They can do even more than anything.
     */
    public static final Role SADMIN;
    
    /**
     * Represents a plot owner. They can do anything.
     */
    public static final Role OWNER;
    
    /**
     * Represents a plot administrator. NOT A SERVER ADMIN!
     */
    public static final Role ADMIN;
    
    /**
     * Represents a plot moderator. They oversee plot duties.
     */
    public static final Role MOD;
    
    /**
     * Represents a plot builder. They can make their own houses etc.
     * This is typically a trusted player.
     */
    public static final Role BUILDER;
    
    /**
     * Represents a plot member. They generally live in the plot but can't do anything special.
     */
    public static final Role MEMBER;
    
    /**
     * Represents a plot guest. Guests can't do anything but enter the plot.
     */
    public static final Role GUEST;
    
    /**
     * Represents a plot outsider. They can't do anything to the plot.
     */
    public static final Role OUTSIDER;
    
    /**
     * Represents the default role.
     */
    public static final Role DEFAULT;
    
    private static final Map<String, Role> roleList = new HashMap<String, Role>();
    private static final Map<Integer, Role> rankList = new HashMap<Integer, Role>();
    
    private final int rank;
    private final String name;
    private final List<Privilege> privileges;
    private final List<Privilege.Category> categories;

    private Role(int rank, String name, List<Privilege> privileges, List<Privilege.Category> categories) {
        this.rank = rank;
        this.name = name;
        this.privileges = privileges;
        this.categories = categories;
    }

    /**
     * Represents the rank of the role in relation to other ranks.
     * Higher is better.
     * 
     * @return The rank
     */
    public int getRank() {
        return rank;
    }

    /**
     * Gets the name of the role.
     * 
     * @return 
     */
    public String getName() {
        return name;
    }
    
    /**
     * Checks if the role contains the specified privilege.
     * 
     * @param privilege
     * @return 
     */
    public boolean hasPrivilege(Privilege privilege) {
        return (privileges.contains(privilege) || categories.contains(privilege.getCategory()));
    }
    
    public Role getPrevious() {
        if (rank == 0) return null;
        return rankList.get(rank - 1);
    }
    
    public Role getNext() {
        return rankList.get(rank + 1);
    }
    
    static {
        //Server admin
        List<Privilege.Category> sadminCategories = new ArrayList<Privilege.Category>();
        sadminCategories.add(Privilege.Category.SADMIN);
        sadminCategories.add(Privilege.Category.OWNER);
        sadminCategories.add(Privilege.Category.ADMIN);
        sadminCategories.add(Privilege.Category.MOD);
        sadminCategories.add(Privilege.Category.BUILDER);
        sadminCategories.add(Privilege.Category.MEMBER);
        sadminCategories.add(Privilege.Category.GUEST);
        SADMIN = new Role(7, "Sadmin", new ArrayList<Privilege>(), sadminCategories);
        roleList.put("sadmin", SADMIN);
        rankList.put(7, SADMIN);
        
        //Owner
        List<Privilege.Category> ownerCategories = new ArrayList<Privilege.Category>();
        ownerCategories.add(Privilege.Category.OWNER);
        ownerCategories.add(Privilege.Category.ADMIN);
        ownerCategories.add(Privilege.Category.MOD);
        ownerCategories.add(Privilege.Category.BUILDER);
        ownerCategories.add(Privilege.Category.MEMBER);
        ownerCategories.add(Privilege.Category.GUEST);
        OWNER = new Role(6, "Owner", new ArrayList<Privilege>(), ownerCategories);
        roleList.put("owner", OWNER);
        rankList.put(6, OWNER);
        
        //Admin
        List<Privilege.Category> adminCategories = new ArrayList<Privilege.Category>();
        adminCategories.add(Privilege.Category.ADMIN);
        adminCategories.add(Privilege.Category.MOD);
        adminCategories.add(Privilege.Category.BUILDER);
        adminCategories.add(Privilege.Category.MEMBER);
        adminCategories.add(Privilege.Category.GUEST);
        ADMIN = new Role(5, "Admin", new ArrayList<Privilege>(), adminCategories);
        roleList.put("admin", ADMIN);
        rankList.put(5, ADMIN);
        
        //Mod
        List<Privilege.Category> modCategories = new ArrayList<Privilege.Category>();
        modCategories.add(Privilege.Category.MOD);
        modCategories.add(Privilege.Category.BUILDER);
        modCategories.add(Privilege.Category.MEMBER);
        modCategories.add(Privilege.Category.GUEST);
        MOD = new Role(4, "Mod", new ArrayList<Privilege>(), modCategories);
        roleList.put("mod", MOD);
        rankList.put(4, MOD);
        
        //Builder
        List<Privilege.Category> builderCategories = new ArrayList<Privilege.Category>();
        builderCategories.add(Privilege.Category.BUILDER);
        builderCategories.add(Privilege.Category.MEMBER);
        builderCategories.add(Privilege.Category.GUEST);
        BUILDER = new Role(3, "Builder", new ArrayList<Privilege>(), builderCategories);
        roleList.put("builder", BUILDER);
        rankList.put(3, BUILDER);
        
        //Member
        List<Privilege.Category> memberCategories = new ArrayList<Privilege.Category>();
        memberCategories.add(Privilege.Category.MEMBER);
        memberCategories.add(Privilege.Category.GUEST);
        MEMBER = new Role(2, "Member", new ArrayList<Privilege>(), memberCategories);
        roleList.put("member", MEMBER);
        rankList.put(2, MEMBER);
        
        //Guest (default)
        List<Privilege.Category> guestCategories = new ArrayList<Privilege.Category>();
        guestCategories.add(Privilege.Category.GUEST);
        GUEST = new Role(1, "Guest", new ArrayList<Privilege>(), guestCategories);
        roleList.put("guest", GUEST);
        rankList.put(1, GUEST);
        
        //Outsider (for admin plots etc)
        OUTSIDER = new Role(0, "Outsider", new ArrayList<Privilege>(), new ArrayList<Privilege.Category>());
        roleList.put("outsider", OUTSIDER);
        rankList.put(0, OUTSIDER);
        
        DEFAULT = GUEST;
    }

    /**
     * Gets a role from a string.
     * 
     * @param name
     * @return 
     */
    public static Role getRole(String name) {
        Role theRole = roleList.get(name.toLowerCase());
        return theRole != null ? theRole : DEFAULT;
    }

    /**
     * Gets a role from an integer.
     * 
     * @param rank The rank
     * @return 
     */
    public static Role getRoleFromRank(int rank) {
        Role theRole = rankList.get(rank);
        return theRole != null ? theRole : DEFAULT;
    }
}
