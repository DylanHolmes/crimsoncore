/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.plot;

import com.crimsonrpg.flaggables.api.FlaggableManager;

/**
 * A plot manager. Currently has nothing out of the ordinary.
 */
public interface PlotManager extends FlaggableManager<Plot> {
}
