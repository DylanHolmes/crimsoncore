/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.plot;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.Flaggable;
import java.util.List;
import org.bukkit.Location;

/**
 *
 * @author simplyianm
 */
public interface Plot extends Flaggable {

    /**
     * Checks if this plot contains the specified location.
     *
     * @param location
     * @return
     */
    public boolean contains(Location location);

    /**
     * Disbands the plot.
     */
    public void disband();

    public List<Plot> getChildren();

    /**
     * Gets the distance between this plot and another location.
     *
     * @param location
     * @return The distance between the plots
     */
    public double getDistance(Location location);

    /**
     * Gets the squared distance between this plot and another location.
     *
     * @param location
     * @return The distance between the plots
     */
    public double getDistanceSquared(Location location);

    /**
     * Gets the unique id of this plot.
     *
     * @return
     */
    public String getId();

    /**
     * Gets the location of this plot.
     *
     * @return Location
     */
    public Location getLocation();

    /**
     * Gets the name of the plot.
     * 
     * @return 
     */
    public String getName();

    /**
     * Gets the immediate parent of this plot.
     *
     * @return Plot Parent
     */
    public Plot getParent();

    /**
     * Gets the size of this plot.
     *
     * @return Size
     */
    public int getSize();

    /**
     * Sets this plots id.
     * 
     * @param id 
     */
    public void setId(String id);
}
