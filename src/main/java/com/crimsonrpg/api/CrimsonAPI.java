/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api;

import com.crimsonrpg.api.citizen.CitizenManager;
import com.crimsonrpg.api.clan.ClanManager;
import com.crimsonrpg.api.plot.PlotManager;
import com.crimsonrpg.api.profession.ProfessionManager;

/**
 *
 * @author simplyianm
 */
public class CrimsonAPI {
	private static CrimsonAPI _instance = new CrimsonAPI();

	private CitizenManager citizenManager = null;

	private ClanManager clanManager = null;

	private PlotManager plotManager = null;

	private ProfessionManager professionManager = null;

	private CrimsonAPI() {
	}

	public static CrimsonAPI getInstance() {
		return _instance;
	}

	public static CitizenManager getCitizenManager() {
		return getInstance().citizenManager;
	}

	public void setCitizenManager(CitizenManager citizenManager) {
		if (this.citizenManager == null) {
			this.citizenManager = citizenManager;
		}
	}

	public static ClanManager getClanManager() {
		return getInstance().clanManager;
	}

	public void setClanManager(ClanManager clanManager) {
		if (this.clanManager == null) {
			this.clanManager = clanManager;
		}
	}

	/**
	 * Gets the plot manager.
	 * 
	 * @return 
	 */
	public static PlotManager getPlotManager() {
		return getInstance().plotManager;
	}

	public void setPlotManager(PlotManager plotManager) {
		if (this.plotManager == null) {
			this.plotManager = plotManager;
		}
	}

	public static ProfessionManager getProfessionManager() {
		return getInstance().professionManager;
	}

	public void setProfessionManager(ProfessionManager professionManager) {
		if (this.professionManager == null) {
			this.professionManager = professionManager;
		}
	}

}
