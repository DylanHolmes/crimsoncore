/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Represents the funds of a plot.
 */
public class FlagPtFunds extends GenericFlag<Plot> {

    private int funds;

    @Override
    public void load(ConfigurationSection section) {
        funds = section.getInt("funds");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("funds.funds", funds);
    }

    @Override
    public void reset() {
        funds = 0;
    }

    public int getFunds() {
        return funds;
    }

    public void setFunds(int funds) {
        this.funds = funds;
    }

    public void add(int amount) {
        funds += amount;
    }

    public void subtract(int amount) {
        funds -= amount;
    }

}
