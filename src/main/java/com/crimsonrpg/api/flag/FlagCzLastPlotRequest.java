/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Last plot request flag.
 */
public class FlagCzLastPlotRequest extends GenericFlag<Citizen> {
    private Plot lastPlot;

    @Override
    public void load(ConfigurationSection section) {}

    @Override
    public void save(ConfigurationSection section) {}

    @Override
    public void reset() {
        lastPlot = null;
    }

    public Plot getLastPlot() {
        return lastPlot;
    }

    public void setLastPlot(Plot lastPlot) {
        this.lastPlot = lastPlot;
    }
    
}
