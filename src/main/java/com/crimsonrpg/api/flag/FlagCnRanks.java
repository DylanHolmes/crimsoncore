/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.util.StringStuff;

/**
 * Contains a clan's ranks.
 * <p />
 * Unfortunately, there are no custom ranks like WoW.
 */
public class FlagCnRanks extends GenericFlag<Clan> {
	/**
	 * Contains clan rank mappings for each player.
	 */
	private Map<String, Rank> ranks = new HashMap<String, Rank>();

	public void load(ConfigurationSection section) {
		for (String key : section.getKeys(false)) {
			ranks.put(key, Rank.valueOf(section.getString(key)));
		}
	}

	public void save(ConfigurationSection section) {
		for (Entry<String, Rank> entry : ranks.entrySet()) {
			section.set(entry.getKey(), entry.getValue().name());
		}
	}

	public void reset() {
		ranks = new HashMap<String, Rank>();
	}

	/**
	 * Gets a list of the names of all of the 
	 * members of the clan, offline or not.
	 * 
	 * @return List of the names of the members.
	 */
	public List<String> getMemberNames() {
		return new ArrayList(ranks.keySet());
	}

	/**
	 * Gets a list of all online members of the clan.
	 * 
	 * @return All clan members.
	 */
	public List<Citizen> getOnlineMembers() {
		List<Citizen> members = new ArrayList<Citizen>();
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (ranks.containsKey(player.getName())) {
				members.add(CrimsonAPI.getCitizenManager().getCitizen((SpoutPlayer) player));
			}
		}
		return members;
	}

	/**
	 * Adds a member to the plot with the specified rank.
	 * 
	 * @param citizen The member of the plot.
	 * @param rank The rank to add as.
	 */
	public void addMember(Citizen citizen, Rank rank) {
		ranks.put(citizen.getName(), rank);
	}

	/**
	 * Gets a citizen's rank in the plot.
	 * 
	 * @param citizen
	 * @return The current rank of the citizen.
	 */
	public Rank getRank(Citizen citizen) {
		return ranks.get(citizen.getName());
	}

	/**
	 * Promotes a citizen in this clan.
	 * 
	 * @param citizen
	 * @return The new rank.
	 */
	public Rank promote(Citizen citizen) {
		Rank rank = getRank(citizen);
		if (rank == null) {
			return rank;
		}
		Rank nextRank = rank.getNext();
		if (nextRank == null) {
			return rank;
		}
		ranks.put(citizen.getName(), nextRank);
		return nextRank;
	}

	/**
	 * Demotes a citizen in this clan.
	 * 
	 * @param citizen
	 * @return The new rank.
	 */
	public Rank demote(Citizen citizen) {
		Rank rank = getRank(citizen);
		if (rank == null) {
			return rank;
		}
		Rank prevRank = rank.getPrevious();
		if (prevRank == null) {
			return rank;
		}
		ranks.put(citizen.getName(), prevRank);
		return prevRank;
	}

	/**
	 * Kicks a citizen out of this flag.
	 * 
	 * @param citizen The citizen to kick.
	 */
	public void kick(Citizen citizen) {
		ranks.remove(citizen.getName());
		citizen.getFlag(FlagCzClan.class).reset();
	}

	/**
	 * Gets the person with founder privileges within the clan.
	 * 
	 * @return The name of the founder.
	 */
	public String getFounderName() {
		for (Entry<String, Rank> entry : ranks.entrySet()) {
			if (entry.getValue().equals(Rank.FOUNDER)) {
				return entry.getKey();
			}
		}

		//Should not happen
		return "";
	}

	/**
	 * Gets the names of all members with the specified rank.
	 * 
	 * @param rank The rank to use
	 * @return List of members with the rank
	 */
	public List<String> getMemberNames(Rank rank) {
		List<String> members = new ArrayList<String>();
		for (Entry<String, Rank> entry : ranks.entrySet()) {
			if (entry.getValue().equals(rank)) {
				members.add(entry.getKey());
			}
		}
		return members;
	}

	/**
	 * Gets the amount of members.
	 * 
	 * @return The member count.
	 */
	public int getMemberCount() {
		return ranks.size();
	}

	/**
	 * Gets the amount of members online.
	 * 
	 * @return The amount of members online.
	 */
	public int getOnlineMemberCount() {
		return getOnlineMembers().size();
	}

	public enum Privilege {
		/**
		 * Permits a player to view the clan's balance.
		 */
		RECRUIT_BALANCE(Rank.RECRUIT),
		/**
		 * Permits a player to chat in the clan chat channel.
		 */
		RECRUIT_CHAT(Rank.RECRUIT),
		/**
		 * Permits a player to deposit money into the plot coffers.
		 */
		RECRUIT_DEPOSIT(Rank.RECRUIT),
		/**
		 * Permits a player to view standard information about the clan.
		 */
		RECRUIT_INFO(Rank.RECRUIT),
		/**
		 * Permits a player to inspect another clan member.
		 */
		RECRUIT_INSPECT(Rank.RECRUIT),
		/**
		 * Permits a player to create plots for their clan.
		 */
		MEMBER_CREATE_PLOT(Rank.MEMBER),
		/**
		 * Permits a player to invite others.
		 */
		MEMBER_INVITE(Rank.MEMBER),
		/**
		 * Permits a player to demote others.
		 */
		OFFICER_DEMOTE(Rank.OFFICER),
		/**
		 * Permits a player to kick others.
		 */
		OFFICER_KICK(Rank.OFFICER),
		/**
		 * Permits a player to promote others.
		 */
		OFFICER_PROMOTE(Rank.OFFICER),
		/**
		 * Permits a player to withdraw from the clan.
		 */
		OFFICER_WITHDRAW(Rank.OFFICER),
		/**
		 * Permits a player to disband a clan.
		 */
		FOUNDER_DISBAND(Rank.FOUNDER),
		/**
		 * Permits a player to rename a clan.
		 */
		FOUNDER_RENAME(Rank.FOUNDER),
		/**
		 * Permits a player to set the clan base.
		 */
		FOUNDER_SET_BASE(Rank.FOUNDER),
		/**
		 * Permits a player to transfer founder rights
		 * of the clan to someone else.
		 */
		FOUNDER_TRANSFER(Rank.FOUNDER);

		private Rank rank;

		private Privilege(Rank rank) {
			this.rank = rank;
		}

		/**
		 * Gets the rank that this privilege is associated with.
		 * 
		 * @return 
		 */
		public Rank getRank() {
			return rank;
		}

	}

	public enum Rank {
		RECRUIT,
		MEMBER,
		VETERAN,
		OFFICER,
		FOUNDER;

		/**
		 * Gets the previous rank.
		 * 
		 * @return The previous rank of the rank.
		 */
		public final Rank getPrevious() {
			return this.ordinal() > 0
					? Rank.values()[this.ordinal() - 1]
					: null;
		}

		/**
		 * Gets the rank after this rank.
		 * 
		 * @return The next rank.
		 */
		public final Rank getNext() {
			return this.ordinal() < (Rank.values().length - 1)
					? Rank.values()[this.ordinal() + 1]
					: null;
		}

		/**
		 * Checks if the rank possesses the given privilege.
		 * 
		 * @param privilege The privilege.
		 * @return Does the rank possess the privilege?
		 */
		public final boolean hasPrivilege(Privilege privilege) {
			return privilege.getRank().ordinal() <= this.ordinal();
		}

		/**
		 * Produces a user-friendly name of the enum.
		 * 
		 * @return The nice name.
		 */
		public final String niceName() {
			return StringStuff.capitalize(this.name());
		}

	}

}
