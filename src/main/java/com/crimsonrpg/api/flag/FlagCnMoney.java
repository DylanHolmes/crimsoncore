/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.flaggables.api.GenericFlag;

/**
 * Contains money.
 */
public class FlagCnMoney extends GenericFlag<Clan> {

    private int money;

    public int getMoney() {
        return money;
    }

    public void add(int amount) {
        money += amount;
    }

    public void subtract(int amount) {
        money -= amount;
    }

    public void setMoney(int amount) {
        money = amount;
    }

    @Override
    public void load(ConfigurationSection section) {
        money = section.getInt("money");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("money", money);
    }

    @Override
    public void reset() {
        money = 0;
    }

}
