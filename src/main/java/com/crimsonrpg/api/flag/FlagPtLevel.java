/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Represents a plot level.
 */
public class FlagPtLevel extends GenericFlag<Plot> {

    private Level level;

    public void setLevel(Level type) {
        this.level = type;
    }

    public Level getLevel() {
        return level;
    }

    public boolean downgrade() {
        Level prevLevel = getPreviousLevel();
        if (prevLevel != null) {
            level = prevLevel;
            return true;
        } else {
            return false;
        }
    }

    public boolean upgrade() {
        Level nextLevel = getNextLevel();
        if (nextLevel != null) {
            level = nextLevel;
            return true;
        } else {
            return false;
        }
    }

    public Level getPreviousLevel() {
        int rank = level.getRank();
        Level previousLevel = Level.fromRank(rank - 1);
        if (previousLevel != null) {
            return previousLevel;
        } else {
            return null;
        }
    }

    public Level getNextLevel() {
        int rank = level.getRank();
        Level nextLevel = Level.fromRank(rank + 1);
        if (nextLevel != null) {
            return nextLevel;
        } else {
            return null;
        }
    }

    @Override
    public void load(ConfigurationSection section) {
        this.level = Level.valueOf(section.getString("level"));
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("level", level.name());
    }

    @Override
    public void reset() {
        level = Level.HOUSE;
    }

    public enum Level {

        /**
         * Represents a house.
         * 
         * Houses are for common people. They should
         * be fairly easy to get; maybe after an hour worth of mining.
         */
        HOUSE(1, "House", 10.0, 1.1, 1, 20, 1000, 1),
        /**
         * Represents a villa.
         * 
         * Villas are for people who are acquainted with the server. After
         * a week of Minecraft, it should be easy to get this.
         * 
         * <p>Improvements:
         * <ul>
         *  <li>NPC Merchants</li>
         * </ul>
         */
        VILLA(2, "Villa", 8.0, 1.2, 15, 50, 5000, 2),
        /**
         * Represents a manor.
         * 
         * A manor is for three players that
         * want to build big.
         */
        MANOR(3, "Manor", 5.0, 1.3, 40, 200, 10000, 3),
        /**
         * Represents a hamlet.
         * 
         * A hamlet is for a small (5-10 people) group of 
         * players who want a small area to build in together.
         * A hamlet is significantly different from a manor.
         * 
         * <p>Improvements:
         * <ul>
         *  <li>Bankers</li>
         *  <li>You can set your spawn point by placing a bed</li>
         *  <li>10 people limit</li>
         * </ul>
         */
        HAMLET(4, "Hamlet", 2.0, 1.4, 50, 200, 20000, 10),
        /**
         * Represents a town.
         * 
         * A town is a serious area for many players (10-30) who
         * need a larger base. Towns are much more useful than cities.
         * 
         * <p>Improvements:
         * <ul>
         *  <li>No more mob spawning</li>
         *  <li>20 people limit</li>
         * </ul>
         */
        TOWN(5, "Town", 1.0, 1.5, 100, 400, 100000, 30),
        /**
         * Represents a city.
         * 
         * A city is a major and versatile hub. This is for
         * many people. (30-100) This means each person had
         * paid about 100 gold ingots to the plot each. :o
         * 
         * <p>Improvements:
         * <ul>
         *  <li>Guards are available</li>
         *  <li>No tax</li>
         * </ul>
         */
        CITY(6, "City", 0.0, 1.8, 200, 1000, 300000, 100),
        /**
         * Represents a nation.
         * 
         * It is near impossible to get one of these.
         * These are for very large groups of players (200+) who
         * want a very large area to cooperate in. You have to be pretty
         * dedicated to get 100,000 gold ingots.
         * 
         * <p>Improvements:
         * <ul>
         *  <li>Unlimited people</li>
         * </ul>
         */
        NATION(7, "Nation", 0.0, 2.0, 800, 2000, 10000000, 100000);

        /**
         * The rank of the plot level.
         */
        private final int rank;

        /**
         * The name of the Level.
         */
        private String name;

        /**
         * Represents the tax rate. Taxes decrease as player responsibility
         * increases. Tax is taken every 24 hours. This rate is taken 
         * off of plot value, which is calculated as follows:
         * 
         * <p><code>plotSize / 2 * influenceMultiplier</code>
         */
        private final double taxRate;

        /**
         * Represents the influence multiplier, used for
         * estimating plot value.
         */
        private final double influenceMultiplier;

        /**
         * The minimum size of a plot with this plot level.
         */
        private final int minSize;

        /**
         * The maximum size of a plot with this plot level.
         */
        private final int maxSize;

        /**
         * The number of coins needed to upgrade to this plot level.
         */
        private final int coinsUpgrade;

        /**
         * The maximum amount of members allowed to be part of the plot.
         */
        private final int maxMembers;

        private Level(int rank, String name, double taxRate, double influenceMultiplier, int minSize, int maxSize, int coinsUpgrade, int maxMembers) {
            this.rank = rank;
            this.name = name;
            this.taxRate = taxRate;
            this.influenceMultiplier = influenceMultiplier;
            this.minSize = minSize;
            this.maxSize = maxSize;
            this.coinsUpgrade = coinsUpgrade;
            this.maxMembers = maxMembers;
        }

        public int getUpgradeCost() {
            return coinsUpgrade;
        }

        public double getInfluenceMultiplier() {
            return influenceMultiplier;
        }

        public int getMaxMembers() {
            return maxMembers;
        }

        public int getMaxSize() {
            return maxSize;
        }

        public int getMinSize() {
            return minSize;
        }

        public String getName() {
            return name;
        }

        public int getRank() {
            return rank;
        }

        public double getTaxRate() {
            return taxRate;
        }

        private static Map<Integer, Level> integerMap = new HashMap<Integer, Level>();

        public static Level fromRank(int integer) {
            return integerMap.get(integer);
        }

        static {
            for (Level type : Level.values()) {
                integerMap.put(type.getRank(), type);
            }
        }

    }

}
