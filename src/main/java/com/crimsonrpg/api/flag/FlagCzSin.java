/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.GenericFlag;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author simplyianm
 */
public class FlagCzSin extends GenericFlag<Citizen> {
	private int sin;

	@Override
	public void load(ConfigurationSection section) {
		sin = section.getInt("sin");
	}

	@Override
	public void save(ConfigurationSection section) {
		section.set("sin", sin);
	}

	@Override
	public void reset() {
		sin = 1000;
	}

	public int getSin() {
		return sin;
	}

	public FlagCzSin add(int amount) {
		sin += amount;
		return this;
	}

	public FlagCzSin subtract(int amount) {
		sin -= amount;
		return this;
	}

}
