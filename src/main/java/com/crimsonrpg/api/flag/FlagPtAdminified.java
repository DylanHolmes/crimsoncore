/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Shows if the plot is adminified or not.
 */
public class FlagPtAdminified extends GenericFlag<Plot> {

    private boolean adminified;

    @Override
    public void load(ConfigurationSection section) {
        adminified = section.getBoolean("adminified");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("adminified", adminified);
    }

    @Override
    public void reset() {
        adminified = false;
    }

    public boolean isAdminified() {
        return adminified;
    }

    public void setAdminified(boolean adminified) {
        this.adminified = adminified;
    }

    public boolean toggle() {
        adminified = !adminified;
        return adminified;
    }

}
