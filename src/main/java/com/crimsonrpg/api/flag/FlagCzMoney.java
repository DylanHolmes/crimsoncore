/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.GenericFlag;

/**
 * Represents the money of a {@link Citizen}.
 */
public class FlagCzMoney extends GenericFlag<Citizen> {

    private int money;

    public void setData(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

    public void add(int amount) {
        money += amount;
    }

    public void subtract(int amount) {
        money -= amount;
    }

    public void set(int amount) {
        money = amount;
    }

    @Override
    public void load(ConfigurationSection section) {
        money = section.getInt("money");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("money", money);
    }

    @Override
    public void reset() {
        money = 0;
    }

}
