package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.personas.personasapi.Personas;
import com.crimsonrpg.personas.personasapi.npc.NPC;
import com.crimsonrpg.personas.personasapi.persona.Persona;
import com.crimsonrpg.api.plot.Plot;
import com.crimsonrpg.util.MD5Helper;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;


/**
 * Contains the npcs of the plot.
 */
public class FlagPtNPCs extends GenericFlag<Plot> {

    private Map<String, NPC> npcs = new HashMap<String, NPC>();

    /**
     * Adds an NPC to the plot.
     * 
     * @param name
     * @param location
     * @param type
     * @return The added NPC
     */
    public NPC addNPC(String name, Location location, Persona persona) {
        String id = MD5Helper.md5(getFlaggable().getId() + MD5Helper.md5(name));
        NPC npc = Personas.getNPCManager().create(id);
        npcs.put(npc.getId(), npc);
        npc.spawn(location);
        return npc;
    }

//    public NPC getNPC(String name) {//Loop through NPCs
//        for (NPC npc : npcs.values()) {
//            if (npc.getName().equalsIgnoreCase(name)) {
//                return npc;
//            }
//        }
//        return null;
//    }

    /**
     * Removes an NPC by name.
     * 
     * @param name
     * @return Is the remove successful?
     */
//    public boolean removeNPC(String name) {
//        NPC theNPC = this.getNPC(name);
//        if (theNPC != null) {
//            npcs.remove(theNPC.getId()).despawn();
//            return true;
//        }
//        return false;
//    }

    @Override
    public void load(ConfigurationSection section) {
        for (String key : section.getKeys(false)) {
//            npcs.put(key, Personas.getNPCManager().getNPC(key));
        }
    }

    @Override
    public void save(ConfigurationSection section) {
        for (NPC npc : npcs.values()) {
            section.set(npc.getId(), true);
        }
    }

    @Override
    public void reset() {
        npcs = new HashMap<String, NPC>();
    }

}
