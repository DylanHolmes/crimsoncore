/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Holds the clan a plot is affiliated with.
 */
public class FlagPtClan extends GenericFlag<Plot> {

    private String clanId;

    private boolean base;

    public void load(ConfigurationSection section) {
        clanId = section.getString("name", "");
        base = section.getBoolean("base", false);
    }

    public void save(ConfigurationSection section) {
        section.set("name", clanId);
        section.set("base", base);
    }

    public void reset() {
        clanId = "";
    }

    public final Clan getClan() {
        if (clanId == null) {
            reset();
        }
        Clan clan = CrimsonAPI.getClanManager().getClan(clanId);
        return clan;
    }

    public final void setClan(Clan clan) {
        clanId = clan.getId();
    }

    public final boolean isBase() {
        return base;
    }

    public final void setBase(boolean base) {
        this.base = base;
    }

}
