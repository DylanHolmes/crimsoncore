/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * The sale price of the plot if it is for sale.
 */
public class FlagPtSalePrice extends GenericFlag<Plot> {

    /**
     * The sale price. -1 means not for sale.
     */
    private int salePrice;

    public int getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(int salePrice) {
        this.salePrice = salePrice;
    }

    @Override
    public void load(ConfigurationSection section) {
        salePrice = section.getInt("price");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("price", salePrice);
    }

    @Override
    public void reset() {
        salePrice = -1;
    }

}
