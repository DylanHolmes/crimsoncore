/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;


import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Represents the founder of a plot.
 * 
 * TODO: time?
 */
public class FlagPtFounder extends GenericFlag<Plot> {

    private String founderType;

    private String name;

    public void setFounderType(String founderType) {
        this.founderType = founderType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setData(String founderType, String name) {
        this.founderType = founderType;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFounderType() {
        return founderType;
    }

    @Override
    public void load(ConfigurationSection section) {
        this.founderType = section.getString("type");
        this.name = section.getString("name");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("type", founderType);
        section.set("name", name);
    }

    @Override
    public void reset() {
        founderType = "";
        name = "";
    }

    @Override
    public String toString() {
        return "FlagFounder{" + "founderType=" + founderType + ", name=" + name + '}';
    }

}
