/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.SessionFlag;

/**
 *
 * @author simplyianm
 */
public class FlagCzCasting extends SessionFlag {
	private boolean casting = false;

	@Override
	public void reset() {
		casting = false;
	}

	public boolean isCasting() {
		return casting;
	}

	public void setCasting(boolean casting) {
		this.casting = casting;
	}

}
