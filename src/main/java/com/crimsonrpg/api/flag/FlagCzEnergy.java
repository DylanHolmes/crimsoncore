/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.GenericFlag;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents citizen energy.
 */
public class FlagCzEnergy extends GenericFlag {
	private int energy;

	@Override
	public void load(ConfigurationSection section) {
		energy = section.getInt("energy");
	}

	@Override
	public void save(ConfigurationSection section) {
		section.set("energy", energy);
	}

	@Override
	public void reset() {
		energy = 0;
	}

	public int getEnergy() {
		return energy;
	}

	public void add(int amount) {
		energy += amount;
	}

	public void subtract(int amount) {
		energy -= amount;
	}

}
