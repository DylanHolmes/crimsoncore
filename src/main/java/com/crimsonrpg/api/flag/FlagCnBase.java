/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.api.CrimsonAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Holds information about the clan base.
 * 
 * TODO: donations will go here: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=BZ8D29KH4S9VQ
 */
public class FlagCnBase extends GenericFlag<Clan> {

    private String pid;

    private Location spawn;

    public void load(ConfigurationSection section) {
        pid = section.getString("pid", "");
        spawn = new Location(
                Bukkit.getWorld(section.getString("world")),
                section.getInt("x"),
                section.getInt("y"),
                section.getInt("z"));
    }

    public void save(ConfigurationSection section) {
        section.set("pid", pid);
        section.set("world", spawn.getWorld().getName());
        section.set("x", spawn.getBlockX());
        section.set("y", spawn.getBlockY());
        section.set("z", spawn.getBlockZ());
    }

    public void reset() {
        pid = "";
    }

    public final Plot getPlot() {
        return CrimsonAPI.getPlotManager().get(pid);
    }

    public final FlagCnBase setPlot(Plot plot) {
        this.pid = plot.getId();
        return this;
    }

    public final Location getSpawn() {
        return spawn;
    }

    public final FlagCnBase setSpawn(Location spawn) {
        this.spawn = spawn;
        return this;
    }

}
