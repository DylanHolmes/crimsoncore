/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Represents the size of a plot.
 */
public class FlagPtSize extends GenericFlag<Plot> {

    private int size;

    @Override
    public void load(ConfigurationSection section) {
        size = section.getInt("size");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("size", size);
    }

    @Override
    public void reset() {
        size = 0;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void expand(int amount) {
        size += amount;
    }

    public void shrink(int amount) {
        size -= amount;
    }

}
