/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents a plot's location.
 */
public class FlagPtLocation extends GenericFlag<Plot> {

    private Location location;

    @Override
    public void load(ConfigurationSection section) {
        String worldString = section.getString("world");
        int locationX = section.getInt("xloc");
        int locationY = section.getInt("yloc");
        int locationZ = section.getInt("zloc");
        location = new Location(Bukkit.getServer().getWorld(worldString), locationX, locationY, locationZ);
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("world", location.getWorld().getName());
        section.set("xloc", location.getBlockX());
        section.set("yloc", location.getBlockY());
        section.set("zloc", location.getBlockZ());
    }

    @Override
    public void reset() {
        location = new Location(null, 0, 0, 0);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
