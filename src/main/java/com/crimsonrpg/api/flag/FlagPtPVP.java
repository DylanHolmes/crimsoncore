/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;

/**
 * Represents a plot pvpstatus.
 */
public class FlagPtPVP extends GenericFlag<Plot> {
    private boolean pvp;

    /**
     * Toggles the PvP status.
     * 
     * @return The new PvP status.
     */
    public boolean toggle() {
        pvp = !pvp;
        return pvp;
    }

    /**
     * Sets the status of PvP enabling in the plot.
     * 
     * @param pvp The status to set.
     */
    public void setPVP(boolean pvp) {
        this.pvp = pvp;
    }

    /**
     * Gets the status of PvP being enabled.
     * 
     * @return True if PvP is enabled.
     */
    public boolean isPVP() {
        return pvp;
    }

    @Override
    public void load(ConfigurationSection section) {
        pvp = section.getBoolean("pvp");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("pvp", pvp);
    }

    @Override
    public void reset() {
        pvp = false;
    }

}
