/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.flaggables.api.GenericFlag;

/**
 * The Clan Name flag.
 */
public class FlagCnName extends GenericFlag<Clan> {

    private String name;

    public void load(ConfigurationSection section) {
        name = section.getString("name");
    }

    public void save(ConfigurationSection section) {
        section.set("name", name);
    }

    public void reset() {
        name = "undefined";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
