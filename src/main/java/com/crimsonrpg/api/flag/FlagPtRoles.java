/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;
import com.crimsonrpg.api.plot.Role;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains the roles of members within the plot.
 */
public class FlagPtRoles extends GenericFlag<Plot> {
	/**
	 * Contains the roles of citizens.
	 */
	private Map<String, Role> roles = new HashMap<String, Role>();

	/**
	 * Resets a citizen's role in the plot.
	 * 
	 * @param citizen
	 * @return 
	 */
	public Role resetRole(Citizen citizen) {
		roles.remove(citizen.getName());
		return getRole(citizen);
	}

	public List<String> getMembers() {
		return new ArrayList<String>(roles.keySet());
	}

	public List<String> getMembers(Role role) {
		List<String> members = new ArrayList<String>();
		for (String member : getMembers()) {
			if (getRole(member).equals(role)) {
				members.add(member);
			}
		}
		return members;
	}

	/**
	 * Gets a role of a citizen.
	 * 
	 * @param citizen
	 * @return 
	 */
	public Role getRole(Citizen citizen) {
		Role role = getRole(citizen.getName());
		if (citizen.getBukkitEntity().hasPermission("crimson.rank.admin")) {
			return Role.SADMIN;
		}

		return role;
	}

	/**
	 * Gets a role of a citizen.
	 * 
	 * @param citizen
	 * @return 
	 */
	public Role getRole(String citizenName) {
		Role theRole = roles.get(citizenName);
		if (theRole == null) {
			return Role.DEFAULT;
		}

		return theRole;
	}

	/**
	 * Sets the role of a citizen for this plot.
	 * 
	 * @param citizen
	 * @param roleEntry 
	 */
	public void setRole(Citizen citizen, Role role) {
		roles.put(citizen.getName(), role);
	}

	/**
	 * Gets the number of defined members in this flag.
	 * 
	 * @return 
	 */
	public int getMemberAmount() {
		return roles.size();
	}

	/**
	 * Promotes a citizen.
	 * 
	 * @param citizen
	 * @return 
	 */
	public boolean promote(Citizen citizen) {
		if (!roles.containsKey(citizen.getName())) {
			return false;
		}
		Role currentRole = getRole(citizen);
		Role nextRole = currentRole.getNext();
		if (nextRole != null) {
			setRole(citizen, nextRole);
			return true;
		}
		return false;
	}

	/**
	 * Demotes a citizen.
	 * 
	 * @param citizen
	 * @return 
	 */
	public boolean demote(Citizen citizen) {
		if (!roles.containsKey(citizen.getName())) {
			return false;
		}
		Role currentRole = getRole(citizen);
		Role previousRole = currentRole.getPrevious();
		if (previousRole != null) {
			setRole(citizen, previousRole);
			return true;
		}
		return false;
	}

	@Override
	public void load(ConfigurationSection section) {
		for (String cName : section.getKeys(false)) {
			roles.put(cName, Role.getRole(section.getString(cName)));
		}
	}

	@Override
	public void save(ConfigurationSection section) {
		for (Entry<String, Role> roleEntry : roles.entrySet()) {
			int value = roleEntry.getValue().getRank();
			if (value >= Role.MEMBER.getRank()
					&& value <= Role.OWNER.getRank()) {
				section.set(roleEntry.getKey(), roleEntry.getValue().getName());
			}
		}
	}

	@Override
	public void reset() {
		roles = new HashMap<String, Role>();
	}

}
