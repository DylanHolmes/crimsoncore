/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.flaggables.api.GenericFlag;
import com.crimsonrpg.api.plot.Plot;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents the name of a plot.
 */
public class FlagPtName extends GenericFlag<Plot> {

    private String name;

    public void setName(String name) {
        this.name = (name.length() > 25) ? name.substring(0, 24) : name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void load(ConfigurationSection section) {
        name = section.getString("name");
    }

    @Override
    public void save(ConfigurationSection section) {
        section.set("name", name);
    }

    @Override
    public void reset() {
        name = "null";
    }

}
