/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.flaggables.api.SessionFlag;

/**
 * Contains the last messaged person.
 */
public class FlagCzLastMessaged extends SessionFlag<Citizen> {

    private String lastMessaged;

    @Override
    public void reset() {
        lastMessaged = "";
    }

    public String getLastMessaged() {
        return lastMessaged;
    }

    public void setLastMessaged(String lastMessaged) {
        this.lastMessaged = lastMessaged;
    }

}
