/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.profession.effect.Effect;
import com.crimsonrpg.flaggables.api.GenericFlag;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Citizen effects.
 */
public class FlagCzEffects extends GenericFlag<Citizen> {
	private Map<String, Integer> effects;

	@Override
	public void load(ConfigurationSection section) {
		Map<String, Object> eff = section.getValues(false);
		effects = new HashMap<String, Integer>();
		for (Entry<String, Object> entry : eff.entrySet()) {
			effects.put(entry.getKey(), Integer.parseInt(entry.getValue().toString()));
		}
	}

	@Override
	public void save(ConfigurationSection section) {
		section.set("effects", effects);
	}

	@Override
	public void reset() {
		effects = new HashMap<String, Integer>();
	}

	public List<Effect> getEffects() {
		List<Effect> r = new ArrayList<Effect>();
		for (String effectId : effects.keySet()) {
			Effect e = CrimsonAPI.getProfessionManager().getEffect(effectId);
			if (e != null) {
				r.add(e);
			}
		}
		return r;
	}

	public void removeEffect(Effect effect) {
		effect.remove((Citizen) getFlaggable());
	}

	public void removeEffectInternal(Effect effect) {
		effects.remove(effect.getId());
	}

	public int getSecondsRemaining(String effectName) {
		Integer time = effects.get(effectName);
		return (time == null) ? -1 : time;
	}

	public int getSecondsRemaining(Effect effect) {
		return getSecondsRemaining(effect.getId());
	}

	/**
	 * Decrements all seconds.
	 * This should be called every tick.
	 */
	public void decrementSeconds() {
		for (Effect effect : getEffects()) {
			int secondsRemaining = getSecondsRemaining(effect);
			--secondsRemaining;
			if (secondsRemaining <= 0) {
				removeEffect(effect);
			}
		}
	}

}
