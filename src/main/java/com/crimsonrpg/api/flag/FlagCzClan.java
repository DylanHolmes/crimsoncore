/*
 * This code is released under the
 * Creative Commons by-nc license.
 * Don't claim this as your own and don't
 * use this code for profit without the permission
 * of the code author.
 */
package com.crimsonrpg.api.flag;

import org.bukkit.configuration.ConfigurationSection;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.flaggables.api.GenericFlag;

/**
 * Contains the String name of the clan.
 */
public class FlagCzClan extends GenericFlag<Citizen> {

    private String clanId;

    public void load(ConfigurationSection section) {
        clanId = section.getString("name", "");
    }

    public void save(ConfigurationSection section) {
        section.set("name", clanId);
    }

    public void reset() {
        clanId = "";
    }

    public final Clan getClan() {
        Clan clan = CrimsonAPI.getClanManager().getClan(clanId);
        if (clan == null) {
            reset();
        }
        return clan;
    }

    public final void setClan(Clan clan) {
        clanId = clan.getId();
    }

}
