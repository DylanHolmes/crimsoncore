/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.flag;

import com.crimsonrpg.api.citizen.Citizen;
import com.crimsonrpg.api.clan.Clan;
import com.crimsonrpg.api.CrimsonAPI;
import com.crimsonrpg.flaggables.api.SessionFlag;

/**
 * Holds the last clan invite of a citizen.
 */
public class FlagCzInviteClan extends SessionFlag<Citizen> {

    private String clan;

    public void reset() {
        clan = null;
    }

    public final Clan getClan() {
        return CrimsonAPI.getClanManager().getClan(clan);
    }

    public final void setClan(Clan clan) {
        this.clan = clan.getId();
    }

}
