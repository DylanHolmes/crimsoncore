/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.item;

/**
 * An Item API. Very simple. (for now, at least)
 * @deprecated Do not add items; we will use Spout when it comes out.
 */
public class ItemAPI {
    private static ItemAPI instance = new ItemAPI();
    
    private CrimsonBlockManager crimsonBlockManager = null;
    private CrimsonItemManager crimsonItemManager = null;
    
    private ItemAPI() {
        
    }
    
    public static ItemAPI getInstance() {
        return instance;
    }
    
    public static CrimsonBlockManager getCrimsonBlockManager() {
        return getInstance().crimsonBlockManager;
    }
    
    public void setCrimsonBlockManager(CrimsonBlockManager crimsonBlockManager) {
        if (this.crimsonBlockManager == null) {
            this.crimsonBlockManager = crimsonBlockManager;
        }
    }
    
    public static CrimsonItemManager getCrimsonItemManager() {
        return getInstance().crimsonItemManager;
    }
    
    public void setCrimsonItemManager(CrimsonItemManager crimsonItemManager) {
        if (this.crimsonItemManager == null) {
            this.crimsonItemManager = crimsonItemManager;
        }
    }
    
    
}
