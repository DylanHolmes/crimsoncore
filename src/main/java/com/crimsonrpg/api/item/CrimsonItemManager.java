/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.item;

import org.getspout.spoutapi.material.CustomItem;

/**
 * The item manager.
 */
public interface CrimsonItemManager {
    public void addItem(String name, CustomItem item);
    
    public CustomItem getItem(String name);
}
