/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.item;

import org.getspout.spoutapi.material.CustomBlock;

/**
 * The crimson block manager.
 */
public interface CrimsonBlockManager {
    public void addBlock(String name, CustomBlock block);
    
    public CustomBlock getBlock(String name);
}
