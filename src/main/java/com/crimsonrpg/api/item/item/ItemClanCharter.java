/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.item.item;

import com.crimsonrpg.core.CrimsonRPG;
import org.getspout.spoutapi.material.item.GenericCustomItem;

import com.crimsonrpg.core.items.CrimsonItems;

/**
 * The clan charter item.
 */
public class ItemClanCharter extends GenericCustomItem {
    public ItemClanCharter(CrimsonRPG plugin) {
        super(plugin, "Clan Charter", CrimsonItems.RESOURCE_BASE + "image/item/clancharter.png");
    }
}
