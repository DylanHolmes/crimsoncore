/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.api.item.item;

import com.crimsonrpg.core.CrimsonRPG;
import com.crimsonrpg.core.items.CrimsonItems;
import org.getspout.spoutapi.material.item.GenericCustomItem;

/**
 * The ban hamma
 */
public class ItemBanHammer extends GenericCustomItem {
    public ItemBanHammer(CrimsonRPG plugin) {
        super(plugin, "Ban Hammer", CrimsonItems.RESOURCE_BASE + "image/item/banhammer.png");
    }
}
