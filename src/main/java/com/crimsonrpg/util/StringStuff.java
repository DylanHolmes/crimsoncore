/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.util;

/**
 * String-related stuff.
 */
public class StringStuff {
    /**
     * Joins an array into a space-delimited string.
     * 
     * @param arr
     * @return 
     */
    public static String join(Object[] arr) {
        return join(arr, " ");
    }
    
    /**
     * Joins an array.
     * 
     * @param arr
     * @param delimiter
     * @return 
     */
    public static String join(Object[] arr, String delimiter) {
        if (arr.length == 0) return "";
        StringBuilder out = new StringBuilder(arr[0].toString());
        for (int i = 1; i < arr.length; i++) out.append(delimiter).append(arr[i]);
        return out.toString();
    }
    
    /**
     * Removes the first arg of an array. Useful for subcommands.
     * 
     * @param arr
     * @return 
     * @author Shade
     */
    public static String[] remFirstArg(String[] arr) {
        return remArgs(arr, 1);
    }
    
    /**
     * Removes args up to a certain point.
     * 
     * @param arr
     * @param startFromIndex
     * @return New args
     */
    public static String[] remArgs(String[] arr, int startFromIndex) {
        if (arr.length == 0) return arr;
        if (arr.length < startFromIndex) return new String[0];
        String[] newSplit = new String[arr.length - startFromIndex];
        System.arraycopy(arr, startFromIndex, newSplit, 0, arr.length - startFromIndex);
        return newSplit;
    }
    
    /**
     * Capitalizes a word.
     * 
     * @param word The word to capitalize.
     * @return The capitalized word.
     */
    public static String capitalize (String word) {
        return (word.length() == 0) ? word : word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }
}
