/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.util;

import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author simplyianm
 */
public abstract class CrimsonCommand implements CommandExecutor {
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        this.execute(cs, cmnd, string, strings);
        return true;
    }
    
    public abstract void execute(CommandSender sender, Command cmnd, String string, String[] args);
    
    /**
     * Registers all given commands.
     */
    public static void registerAll(Map<String, CrimsonCommand> commands, JavaPlugin plugin) {
        for (Entry<String, CrimsonCommand> command : commands.entrySet()) {
            plugin.getCommand(command.getKey()).setExecutor(command.getValue());
        }
    }
}
